resource random_id "airflow_local_pvc" {
  byte_length = 3
}

resource kubernetes_storage_class "manual" {
  metadata {
    name = "hostpath-${random_id.airflow_local_pvc.hex}"
  }
  storage_provisioner = "docker.io/hostpath"
}

resource kubernetes_persistent_volume "airflow_local_pv" {
  metadata {
    name = "airflow-local-pv-${random_id.airflow_local_pvc.hex}"
  }
  spec {
    storage_class_name = kubernetes_storage_class.manual.metadata[0].name
    persistent_volume_reclaim_policy = "Delete"
    capacity = {
      storage = "2Gi"
    }
    access_modes = ["ReadWriteMany"]
    persistent_volume_source {
      host_path {
        path = abspath(path.module)
        type = "Directory"
      }
    }
  }
}

resource kubernetes_persistent_volume_claim "airflow_local_pvc" {
  metadata {
    name = "airflow-local-pvc-${random_id.airflow_local_pvc.hex}"
  }
  spec {
    volume_name = kubernetes_persistent_volume.airflow_local_pv.metadata[0].name
    storage_class_name = kubernetes_storage_class.manual.metadata[0].name
    access_modes = ["ReadWriteMany"]
    resources {
      requests = {
        storage = "1Gi"
      }
    }
  }
}

