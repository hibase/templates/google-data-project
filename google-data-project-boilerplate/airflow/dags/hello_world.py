from datetime import datetime
from airflow import DAG
from airflow.operators.dummy import DummyOperator
from airflow.operators.python import PythonOperator
from airflow.models.variable import Variable

def say_hello(**kwargs):
    if kwargs['dag_run'].conf.get('fail'):
        raise 'fail on demand'
    return Variable.get('hello-world')

dag = DAG(
        'hello_world',
        description='Hello world example',
        schedule_interval='@once',
        start_date=datetime(1987, 12, 3),
        catchup=False,
        default_args={
            'email_on_failure': True,
            'email': 'lukas@hibase.co',
        },
        tags=['kind:test'])

with dag:
    dummy_operator = DummyOperator(task_id='dummy_task', retries = 3)

    hello_operator = PythonOperator(task_id='hello_task', provide_context=True, python_callable=say_hello)

    dummy_operator >> hello_operator

