# Airflow

## Local setup

Involved technologies: Shell, Docker, Google Cloud SDK, Terraform.

### macOS

* Install Google Cloud SDK using your preferred method. e.g. https://cloud.google.com/sdk/docs/install#mac

    > Note: Make sure you have followed any post-installation instructions properly.

* Initialize gcloud:

  ```
  $ gcloud init
  ```

* Setup your application default login:

  ```
  $ gcloud auth application-default login
  ```

* Clone this repository using Google Cloud SDK

    ```
    $ gcloud source repos clone data/airflow --project=
    ```

* Install Docker for Mac https://www.docker.com/products/docker-desktop

    > Note: There is a bug in the gRPC FUSE supported filesystem in Docker for Mac. You need to change back
      to "osxfs". Disable "Use gRPC FUSE for file sharing" in "Preferences > Experimental Features".

    While there, also activate the Kubernetes cluster in "Preferences > Kubernetes", enable the option "Enable Kubernetes".

* Install direnv

    ```
    $ brew install direnv
    ```

  Make sure you also complete the post-installation instructions of `direnv`. These might vary depending on your shell.
  See also: https://direnv.net/docs/hook.html

* Install terraform

    ```
    $ brew install terraform
    ```

* Copy `.envrc.example` to `.envrc`, modify to point to your `.kube/config` that connects to the Kubernetes on Docker for Mac.

    The default, pointing to `~/.kube/config` should be fine.

    ```
    $ cp .envrc.example .envrc
    ```

* Activate the `.envrc` using `direnv`

    ```
    $ direnv allow
    ```

* Initialize Terraform on your local setup.

    > Note: This must only be done on first setup or when a new module is added to the setup.

    ```
    $ terraform init
    ```

* Apply the Terraform setup

    ```
    $ terraform apply
    ```

    > Note: Terraform will present you with a plan of changes, confirm that plan with "yes".

## General instructions

### Accessing the Airflow web ui

Once you have started the Airflow cluster, you can open it at http://localhost:3000.

Make sure you have created the first admin user.
Enter a running Airflow container and execute:

```
$ airflow users create --role Admin --firstname First --lastname Last --email admin@example.com --password changme
```

The setup is configured to launch the service at a `NodePort` on port 3000.

### Adding connections or variables

Connections are managed by Google Secrets Manager, therefore you need the necessary privileged to
access the service account at the time of the Terraform setup.

> Note: Do not create or store secrets locally or in the repository. We use gcloud authentication for
a reason, and Google Secrets Manager is a convenient way to share secrets across development environments.

Login to GCP, navigate to "Security > Secrets Manager".

All secrets prefixed with `af-conn` and `af-var` are available to Airflow locally and in production.
Those are fetched without the Secrets Manager prefix, e.g.

A secret in `af-var-hello-world` containing the payload `Hello World` can be accessed in Airflow
using:

```python
from airflow.models.variable import Variable

Variable.get('hello-world')
# => returns 'Hello World', reading the payload from "af-var-hello-world" in the Google Secrets Manager.
```

### Following logs from Airflow

Logs are synchronised back from the containers to your local directory in `logs/`.
You can find logs for a specific DAG/task/run in `logs/<name of dag>/<name of task>/`.

### Accessing the Airflow metadata/Postgres database

You can `kubectl port-forward svc/postgresql 5432` to expose the Postgres metadata database to
your local computer.

The superuser username is `postgres` and the password can be obtained through Terraform:

```
$ terraform output postgres__superuser_password
```

The normal username and password can also be obtained through Terraform:

```
$ terraform output postgres__username
$ terraform output postgres__password
$ terraform output postgres__database
```

### Rebuilding the docker containers

There is a `Makefile` to simplify the process of rebuilding the images.

```
$ make build
```

### Restarting/cycling the Airflow pods

Sometimes you want to restart the Airflow pods, without re-creating the whole setup.
This is also reflected in the `Makefile`:

```
$ make restart
```

### Starting from scratch

If you want to redo the setup, you can re-create everything using Terraform.

> Note: You must also delete the Postgres metadata database storage before re-creating resources,
otherwise you will have the previous data.

To destroy the local setup, issue the following commands:

```
$ terraform destroy
$ kubectl delete pvc data-airflow-postgresql-0
```

To build the cluster up again, issue the following command:

```
$ terraform apply
```

## Local workflow

* Dags are to be found in the `dags/` directory in this repository.

    > Note: These are mounted directly into Airflow running on your local Kubernetes.

* Create a feature branch

* Work and test your changes in Airflow

    * Run the Airflow web ui

        ```
        $ kubectl port-forward svc/airflow 8080
        ```

    * Enter a shell running Airflow

        ```
        $ kubectl exec -ti $(kubectl get po -lapp.kubernetes.io/component=web -ojsonpath={.items[0].metadata.name})
        > cd /opt/bitnami/airflow
        ```

* Review the data

* Commit your changes to the feature branch

* Create a merge request

* Merge the peer reviewed merge request back into master.

    The code on production will be regularly updated from the master branch.

