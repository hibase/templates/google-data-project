variable "kubeconfig_context" {
  type        = string
  default     = "docker-desktop"
  description = "The kubeconfig context to use for this setup."
}

variable "target_service_account" {
  type        = string
  description = "The service account to use for Google Cloud operations"
}

variable "target_project_id" {
  type        = string
  description = "The Google Cloud project id to target"
}

variable "target_region" {
  type        = string
  default     = "europe-west1"
  description = "The Google Cloud project id to target"
}

