terraform {
  required_version = "~>0.14.6"
  required_providers {
    google     = "~>3.47"
    kubernetes = "~>1.13.3"
    helm       = "~>1.3.2"
  }
}

provider kubernetes {
  config_context   = var.kubeconfig_context
  load_config_file = true
}

provider helm {
  kubernetes {
    config_context   = var.kubeconfig_context
    load_config_file = true
  }
}

provider google {
  alias   = "token_generator"
}

data google_client_config "default" {
  provider = google.token_generator
}

data google_service_account_access_token "terraform" {
  provider = google.token_generator

  target_service_account = var.target_service_account
  lifetime               = "1200s"
  scopes                 = [
    "https://www.googleapis.com/auth/cloud-platform"
  ]
}

provider google {
  project      = var.target_project_id
  access_token = data.google_service_account_access_token.terraform.access_token
  region       = var.target_region
}

