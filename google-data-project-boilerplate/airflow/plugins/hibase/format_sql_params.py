import re

OPTIONAL_RE = re.compile(r'\[\[[^\[\]]+\]\]')
REPLACE_RE = re.compile(r'\$\$[a-zA-Z0-9_-]+')
PLACEHOLDER_RE = re.compile(r'\$[a-zA-Z0-9_-]+')
SECTIONS_RE = re.compile(r'([^\[\]]*\$\$?[a-zA-Z0-9_-]+)+|(\[\[[^\[\]]+\]\])')

def format_sql(sql, **params):
    (sql, params) = format_sql_params(sql, **params)
    if len(params) > 0:
        raise ArgumentError('This query has placeholder params, use $$direct_var or format_sql_params(sql, **params)')
    return sql

def format_sql_params(sql, **params):
    args = []
    def _format(section, **params):
        section_args = []
        while True:
            match = OPTIONAL_RE.search(section)
            if not match:
                break
            optional = match.group()
            optional = optional[2:len(optional)-2]
            try:
                optional, targs = _format(optional, **params)
            except KeyError:
                targs = []
                optional = ''
            section_args = section_args + list(targs)
            return (section[:match.start()] + optional + section[match.end():], section_args)
        while True:
            match = REPLACE_RE.search(section)
            if not match:
                break
            key = match.group()
            key = key[2:len(key)]
            value = params[key]
            section = section[:match.start()] + params[key] + section[match.end():]
        while True:
            match = PLACEHOLDER_RE.search(section)
            if not match:
                break
            key = match.group()
            key = key[1:len(key)]
            value = params[key]
            section = section[:match.start()] + '?' + section[match.end():]
            section_args.append(value)
        return (section, section_args)

    while True:
        match = SECTIONS_RE.search(sql)
        if not match:
            break
        section = match.group()
        section, targs = _format(section, **params)
        sql = sql[:match.start()] + section + sql[match.end():]
        args = args + list(targs)
    # return the new sql query along with a tuple of the params for cursor.execute(query, *params)
    return (sql, tuple(args))

