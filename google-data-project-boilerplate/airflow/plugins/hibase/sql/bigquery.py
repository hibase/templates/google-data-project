APPEND_FROM_TABLE = """
INSERT INTO $$target_table
SELECT * FROM $$source_table
"""

CREATE_IF_NOT_EXISTS_FROM_TABLE = """
CREATE TABLE IF NOT EXISTS $$target_table
[[PARTITION BY $$partition_by]]
AS SELECT * FROM $$source_table LIMIT 1
"""

CREATE_OR_REPLACE_MERGED_TABLE = """
CREATE OR REPLACE TABLE $$target_table AS
SELECT
  * EXCEPT(row_number)
FROM (
  SELECT
    *,
    ROW_NUMBER() OVER (PARTITION BY $$identifier_column ORDER BY $$last_modified_column DESC) row_number
  FROM
    (
      SELECT * FROM $$target_table
      UNION ALL
      SELECT * FROM $$source_table
    )
)
WHERE row_number = 1
"""

DROP_TABLE = "DROP TABLE $$table_name"

MAX_VALUE_AS_DATETIME = "SELECT CAST(MAX($$column) AS DATETIME) FROM $$table_name"

