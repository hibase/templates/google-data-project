import hashlib

def sha256(value):
    if isinstance(value, bytes):
        value_b = value
    else:
        value_b = bytes(str(value), 'utf-8')
    sha256 = hashlib.sha256()
    sha256.update(value_b)
    return sha256.hexdigest()

