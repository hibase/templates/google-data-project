import os
import re
import tempfile
import csv
import json

from airflow.configuration import conf
from airflow.exceptions import AirflowException
from airflow.models import BaseOperator
from airflow.utils.decorators import apply_defaults
from airflow.providers.sftp.hooks.sftp import SFTPHook
from airflow.providers.google.cloud.hooks.gcs import GCSHook
from hibase.airflow_utils import normalize_name

class SftpCsvToGcs(BaseOperator):
    """
    Copy data from an SFTP server, and upload them to GCS.
    """

    template_fields = ('remote_full_path','gcs_destination_path','gcs_schema_path')
    template_ext = ('.txt',)

    @apply_defaults
    def __init__(self,
        *,
        sftp_conn_id,
        remote_full_path,
        gcs_destination_path,
        gcs_schema_path,
        bucket,
        schema_hints={},
        csv_opts={},
        gcs_conn_id='google_cloud_default',
        **kwargs
    ):
        super(SftpCsvToGcs, self).__init__(**kwargs)
        self.sftp_conn_id = sftp_conn_id
        self.remote_full_path = remote_full_path
        self.schema_hints = schema_hints
        self.csv_opts = csv_opts
        self.gcs_destination_path = gcs_destination_path
        self.gcs_schema_path = gcs_schema_path
        self.bucket = bucket
        self.gcs_conn_id = gcs_conn_id

    def execute(self, context):
        sftp_hook = SFTPHook(ftp_conn_id=self.sftp_conn_id)
        # Note: SFTPHook is suffering from the usual Airflow code insanity.
        #       They advertise the SSHHook connection settings (e.g. providing a private key file),
        #       but then deviate from that in the implementation of the SFTPHook. Pathetic.
        if sftp_hook.key_file.startswith('-----BEGIN OPENSSH PRIVATE KEY-----'):
            key_file = tempfile.NamedTemporaryFile('w', delete=True)
            key_file.write(sftp_hook.key_file)
            key_file.flush()
            sftp_hook.key_file = key_file.name

        with tempfile.NamedTemporaryFile() as tmpfile:
            basename = os.path.basename(self.remote_full_path)
            local_path = tmpfile.name
            sftp_hook.retrieve_file(self.remote_full_path, local_path)

            schema = self.bq_schema_for_csv(local_path, self.schema_hints, self.csv_opts)
            self.upload_schema(schema)
            local_path, mime_type = self.bq_data_from_csv(local_path, self.csv_opts)
            self.upload_to_gcs(local_path, self.gcs_destination_path, mime_type=mime_type)

    def bq_data_from_csv(self, csvpath, csv_opts):
        return csvpath, 'text/csv'

    def bq_schema_for_csv(self, csvpath, schema_hints, csv_opts):
        schema = {}
        with open(csvpath, 'r') as file:
            csv_reader = csv.reader(file, **csv_opts)
            # Note: At the time we assume first row is a list of column headers
            for row in csv_reader:
                for col in row:
                    normalized_column_name = normalize_name(col)
                    schema[col] = {
                        'name': normalized_column_name,
                        'type': schema_hints.get(normalized_column_name, 'STRING'),
                        'mode': 'NULLABLE'
                    }
                break
        self.log.info('Acquired schema from {}: {}'.format(csvpath, schema))
        return list(schema.values())

    def upload_schema(self, schema):
        with tempfile.NamedTemporaryFile(delete=True) as tmp_file:
            schema_str = json.dumps(schema, sort_keys=True)
            schema_str = schema_str.encode('utf-8')
            tmp_file.write(schema_str)
            tmp_file.flush()
            self.upload_to_gcs(tmp_file.name, self.gcs_schema_path, mime_type='application/json')

    def upload_to_gcs(self, localpath, destpath, mime_type):
        hook = GCSHook(gcp_conn_id=self.gcs_conn_id)
        hook.upload(self.bucket, destpath, localpath, mime_type=mime_type, gzip=False)

