from typing import Optional
from paramiko import SFTP_NO_SUCH_FILE
from tempfile import NamedTemporaryFile
from airflow.providers.sftp.hooks.sftp import SFTPHook
from airflow.sensors.base import BaseSensorOperator
from airflow.utils.decorators import apply_defaults


class SftpSensor(BaseSensorOperator):
    """
    Waits for a file or directory to be present on SFTP.

    :param remote_full_path: Remote file or directory path
    :type remote_full_path: str
    :param sftp_conn_id: The connection to run the sensor against
    :type sftp_conn_id: str
    """

    template_fields = ('remote_full_path',)

    @apply_defaults
    def __init__(self, *, remote_full_path: str, sftp_conn_id: str = 'sftp_default', **kwargs) -> None:
        super().__init__(**kwargs)
        self.remote_full_path = remote_full_path
        self.sftp_conn_id = sftp_conn_id

    def poke(self, context: dict) -> bool:
        sftp_hook = SFTPHook(ftp_conn_id=self.sftp_conn_id)
        # Note: SFTPHook is suffering from the usual Airflow code insanity.
        #       They advertise the SSHHook connection settings (e.g. providing a private key file),
        #       but then deviate from that in the implementation of the SFTPHook.
        if sftp_hook.key_file.startswith('-----BEGIN OPENSSH PRIVATE KEY-----'):
            key_file = NamedTemporaryFile('w', delete=True)
            key_file.write(sftp_hook.key_file)
            key_file.flush()
            sftp_hook.key_file = key_file.name
        self.log.info('Poking for %s', self.remote_full_path)
        try:
            sftp_hook.get_mod_time(self.remote_full_path)
        except OSError as e:
            if e.errno != SFTP_NO_SUCH_FILE:
                raise e
            return False
        sftp_hook.close_conn()
        return True

