import decimal
import json
import abc
import json
import os
import sys
import random
import string
import unicodecsv as csv
import psycopg2
from tempfile import NamedTemporaryFile
from Cryptodome.Cipher import AES
from base64 import b64encode, b64decode

from airflow.utils.decorators import apply_defaults
from airflow.providers.postgres.hooks.postgres import PostgresHook
from airflow.hooks.base import BaseHook
from airflow.models import BaseOperator
from hibase.format_sql_params import format_sql_params
from hibase.pii_encryption_mixin import generate_public_ref, encrypt, decrypt


class BaseSqlEncryptPiisToPostgresqlCSVTempfile(BaseOperator):
    """
    Copy data from a SQL query, write it to a CSV file.
    When specifying pii_ref_name, pii_ref_column and pii_columns it will create a secret key
    per pii_ref_name, pii_ref_column pair and encrypt all pii_columns in all rows with the respective key.

    The references and keys are stored in a postgres database, specified as postgres_conn_id.
    """

    template_fields = ('sql', 'query_params', 'keybase_schema', 'keybase_table_name', 'pii_ref_name', 'pii_ref_column')
    template_ext = ('.sql',)
    ui_color = '#e0a98c'

    @apply_defaults
    def __init__(self,
                 source_conn_id,
                 sql,
                 query_params={},
                 offset_column=None,
                 pii_ref_name=None,
                 pii_ref_column=None,
                 pii_columns=[],
                 postgres_conn_id='postgres_default',
                 transform=None,
                 batch_error_handler=None,
                 schema=None,
                 field_delimiter=',',
                 keybase_schema='public',
                 keybase_table_name='secrets',
                 master_key=None,
                 approx_max_file_size_bytes=256000000,
                 **kwargs):
        super().__init__(**kwargs)
        self.source_conn_id = source_conn_id
        self.sql = sql
        self.query_params = query_params
        self.offset_column = offset_column
        self.pii_ref_name = pii_ref_name
        self.pii_ref_column = pii_ref_column
        self.pii_columns = pii_columns
        self.postgres_conn_id = postgres_conn_id
        self.transform = transform
        self.batch_error_handler = batch_error_handler
        self.schema = schema
        self.field_delimiter = field_delimiter
        self.keybase_schema = keybase_schema
        self.keybase_table_name = keybase_table_name
        self.master_key = master_key
        self.approx_max_file_size_bytes = approx_max_file_size_bytes

    def execute(self, context):
        self.ensure_postgres_schema()
        self.batches_start()
        for data_file in self.generate_batches():
            self.handle_data_file(data_file)
        self.batches_done()

    def batches_start(self):
        pass

    def handle_error_in_batch(self, err, retries):
        if not self.batch_error_handler:
            return
        return self.batch_error_handler(err, retries)

    def handle_data_file(self, fileobj):
        fileobj.close()

    def batches_done(self):
        pass

    def ensure_postgres_schema(self):
        extension_ddl = "CREATE EXTENSION IF NOT EXISTS pgcrypto"
        schema_ddl = "CREATE SCHEMA IF NOT EXISTS {schema}".format(schema=self.keybase_schema)
        table_ddl = """
        CREATE TABLE IF NOT EXISTS {schema}.{table_name} (
            ref_name    text       NOT NULL,
            private_ref text       NOT NULL,
            public_ref  text       NOT NULL,
            secret      text       DEFAULT substring(regexp_replace(encode(gen_random_bytes(64), 'BASE64'), %(crlf)s, '', 'g') for 32) CHECK (length(secret) = 32),
            created_at  timestamp  NOT NULL DEFAULT now(),
            revoked_at  timestamp,
            PRIMARY KEY (ref_name, private_ref),
            CONSTRAINT unique_public_ref UNIQUE(ref_name, public_ref)
        )
        """.format(schema=self.keybase_schema, table_name=self.keybase_table_name)
        hook = PostgresHook(postgres_conn_id=self.postgres_conn_id)
        conn = hook.get_conn()
        cursor = conn.cursor()
        try:
            cursor.execute(extension_ddl)
            cursor.execute(schema_ddl)
            cursor.execute(table_ddl, {'crlf': '[\r\n]'})
            conn.commit()
        finally:
            conn.close()
            cursor.close()

    def transform_columns(self, schema, row):
        if not self.transform:
            return row
        for column_name, call in self.transform.items():
            column_index = schema.index(column_name)
            row[column_index] = call(row[column_index])
        return row

    def create_temporary_csv(self):
        """
        Configure a CSV writer, write the header line and return both the file handle and the csv writer
        """
        file_handle = NamedTemporaryFile(delete=False)
        csv_writer = csv.writer(file_handle, encoding='utf-8', delimiter=self.field_delimiter)
        return (file_handle, csv_writer)

    def encrypt_row(self, pg_conn, schema, row):
        if len(self.pii_columns) == 0:
            return row

        public_ref, secret, is_revoked = self.get_or_create_secret(pg_conn, schema, row)
        # replace the private ref with the public ref
        ref_index = schema.index(self.pii_ref_column)
        row[ref_index] = public_ref
        # pii reference got revoked, mark all fields as revoked
        if is_revoked:
            return revoke_row(schema, row)

        for col_index, column_name in enumerate(schema):
            # column does not need to be encrypted
            if column_name not in self.pii_columns:
                continue
            # value is NULL
            if row[col_index] is None:
                continue
            # value is an empty string
            if row[col_index] == '':
                row[col_index] = '(empty)'
                continue
            # value must be encrypted, coerce to string
            data = str(row[col_index])
            # encrypt
            encrypted = encrypt(public_ref, data, secret)
            # replace
            row[col_index] = encrypted
        return row

    def get_or_create_secret(self, pg_conn, schema, row):
        retries = 2
        ref_index = schema.index(self.pii_ref_column)
        private_ref = row[ref_index]
        public_ref = generate_public_ref()
        sql_params = {'ref_name': self.pii_ref_name, 'private_ref': private_ref, 'public_ref': public_ref}
        cursor = pg_conn.cursor()
        result = None
        while True:
            select_sql = """
            SELECT public_ref, secret, revoked_at
            FROM {schema}.{table_name}
            WHERE ref_name = %(ref_name)s AND private_ref = %(private_ref)s::text
            """
            select_sql = select_sql.format(schema=self.keybase_schema, table_name=self.keybase_table_name)
            cursor.execute(select_sql, sql_params)
            result = cursor.fetchone()
            if result:
                break
            insert_sql = """
            INSERT INTO {schema}.{table_name}
                   (ref_name,     private_ref,     public_ref)
            VALUES (%(ref_name)s, %(private_ref)s, %(public_ref)s)
            RETURNING public_ref, secret, revoked_at
            """
            insert_sql = insert_sql.format(schema=self.keybase_schema, table_name=self.keybase_table_name)
            try:
                cursor.execute(insert_sql, sql_params)
            except psycopg2.IntegrityError:
                pg_conn.rollback()
                retries -= 1
                if retries > 0:
                    continue
                else:
                    raise
            else:
                result = cursor.fetchone()
                pg_conn.commit()
                break
        cursor.close()
        return (result[0], result[1], result[2] is not None)

    def revoke_row(schema, row):
        for col_index, column_name in enumerate(schema):
            if column_name not in self.pii_columns:
                continue
            row[col_index] = '(revoked)'
        return row

    def get_offset_value(self, schema, row):
        if self.offset_column:
            return row[schema.index(self.offset_column)]
        else:
            return None

    def generate_batches(self):
        """
        Takes a cursor, and writes results to a local file.
        Yields each batch after it exceeded the threshold of approx_max_file_size_bytes.
        """
        retries = 0
        self.comitted_offset = 0
        self.committed_offset_value = None
        # retry loop if self.handle_error_in_batch(...) returns True
        while True:
            try:
                # setup a new batch
                batch_offset = 0
                tmp_file, tmp_file_csv_writer = self.create_temporary_csv()
                pg_conn = PostgresHook(postgres_conn_id=self.postgres_conn_id).get_conn()
                cursor = self.query(offset=self.comitted_offset, offset_value=self.committed_offset_value)
                self.last_cursor = cursor
                schema = list(map(lambda schema_tuple: schema_tuple[0], cursor.description))
                tmp_file_csv_writer.writerow(schema)
                col_type_dict = self.get_col_type_dict()
                # scroll through rows
                for row in cursor:
                    row = list(row)
                    row = self.transform_columns(schema, row)
                    row = self.convert_types(schema, col_type_dict, row)
                    row = self.encrypt_row(pg_conn, schema, row)
                    tmp_file_csv_writer.writerow(row)
                    batch_offset += 1

                    # yield a batch if the file exceeds the file size limit.
                    if tmp_file.tell() >= self.approx_max_file_size_bytes:
                        try:
                            tmp_file.flush()
                            yield tmp_file
                        finally:
                            self.comitted_offset += batch_offset
                            self.committed_offset_value = self.get_offset_value(schema, row)
                            batch_offset = 0
                            tmp_file.close()
                        tmp_file, tmp_file_csv_writer = self.create_temporary_csv()
                        tmp_file_csv_writer.writerow(schema)
                # handle the last batch
                # there might be a temp file outstanding, that has not yet reached the file size limit
                if not tmp_file.closed:
                    tmp_file.flush()
                    yield tmp_file
                    tmp_file.close()
            # certain errors (e.g. connection loss) can be retried knowing the last committed offset
            except:
                (_err_type, err, _traceback) = sys.exc_info()
                self.log.warning('Encountered an error: %s' % (err,))
                if self.handle_error_in_batch(err, retries):
                    # clean up previous tmp file to have constant disk space usage
                    if os.path.exists(tmp_file.name):
                        os.unlink(tmp_file.name)
                    tmp_file.close()
                    retries += 1
                    self.log.info('Retrying after error, attempt: %s' % (retries,))
                    continue
                else:
                    self.log.warning('Giving up after %s attempts.' % (retries,))
                    raise
            # close the postgres connection
            pg_conn.close()
            # end the retry loop
            break

    def query(self, offset=0, offset_value=None):
        connection = BaseHook.get_connection(self.source_conn_id)
        hook = connection.get_hook()
        conn = hook.get_conn()
        cursor = conn.cursor()
        query_params = dict(self.query_params)
        if self.offset_column:
            query_params['offset_column'] = self.offset_column
        if offset_value and self.offset_column:
            query_params['offset_value'] = offset_value
        elif offset:
            query_params['offset'] = offset
        sql, params = format_sql_params(self.sql, **query_params)
        self.log.info('Executing query with params: {}\n{}'.format(params, sql))
        cursor.execute(sql, *params)
        return cursor

    def convert_types(self, schema, col_type_dict, row):
        """Convert values from DBAPI to output-friendly formats."""
        return [
            self.convert_type(value, col_type_dict.get(name))
            for name, value in zip(schema, row)
        ]

    @classmethod
    def convert_type(cls, value, schema_type):
        """
        Takes a value from MSSQL, and converts it to a value that's safe for
        JSON/Google Cloud Storage/BigQuery.
        """
        if isinstance(value, decimal.Decimal):
            return float(value)
        return value

    def get_col_type_dict(self):
        """
        Return a dict of column name and column type based on self.schema if not None.
        """
        schema = []
        if isinstance(self.schema, str):
            schema = json.loads(self.schema)
        elif isinstance(self.schema, list):
            schema = self.schema
        elif self.schema is not None:
            self.log.warning('Using default schema due to unexpected type.'
                             'Should be a string or list.')

        col_type_dict = {}
        try:
            col_type_dict = {col['name']: col['type'] for col in schema}
        except KeyError:
            self.log.warning('Using default schema due to missing name or type. Please '
                             'refer to: https://cloud.google.com/bigquery/docs/schemas'
                             '#specifying_a_json_schema_file')
        return col_type_dict
