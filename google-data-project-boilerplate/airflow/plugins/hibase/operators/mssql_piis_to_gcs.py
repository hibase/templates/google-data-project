from multiprocessing import Process
from tempfile import NamedTemporaryFile
from time import sleep
import os
import re
import json
import pyodbc
# importing only for type matching
import datetime
import decimal

from airflow.utils.decorators import apply_defaults
from airflow.providers.google.cloud.hooks.gcs import GCSHook
from airflow.hooks.base import BaseHook

from hibase.operators.base_sql_encrypt_piis_to_postgresql_csv_tempfile import BaseSqlEncryptPiisToPostgresqlCSVTempfile


def upload_to_gcs(gcs_conn_id, bucket, file_name, path, mime_type):
    hook = GCSHook(gcp_conn_id=gcs_conn_id)
    hook.upload(bucket, path, file_name, mime_type=mime_type, gzip=False)

class MssqlPiisToGcs(BaseSqlEncryptPiisToPostgresqlCSVTempfile):

    template_fields = ('sql', 'query_params', 'bucket', 'filename', 'schema_filename', 'schema')

    type_map = {
        str: 'STRING',
        int: 'INTEGER',
        datetime.datetime: 'TIMESTAMP',
        datetime.date: 'DATE',
        datetime.time: 'TIME',
        bool: 'BOOLEAN',
        bytes: 'BYTES',
        decimal.Decimal: 'NUMERIC',
    }

    @apply_defaults
    def __init__(self,
            *,
            bucket,
            filename,
            schema_filename=None,
            gcs_conn_id='google_cloud_default',
            upload_timeout=3600,
            resume_on_error=True,
            **kwargs):
        super(MssqlPiisToGcs, self).__init__(**kwargs)
        self.bucket = bucket
        self.filename = filename
        self.schema_filename = schema_filename
        self.gcs_conn_id = gcs_conn_id
        self.upload_timeout = upload_timeout
        self.resume_on_error = resume_on_error

    def batches_start(self):
        self.file_index = 0
        self.upload_processes = []
        pass

    def handle_error_in_batch(self, err, retries):
        if not self.resume_on_error:
            return False
        if isinstance(err, pyodbc.OperationalError):
            sleep(retries * 10)
            return retries <= 5
        return False

    def handle_data_file(self, fileobj):
        path = self.filename.format(self.file_index)
        self.file_index += 1
        self.close_finished_uploads()
        proc = Process(target=upload_to_gcs, args=(self.gcs_conn_id, self.bucket, fileobj.name, path, 'text/csv'))
        proc.start()
        self.upload_processes.append((fileobj, proc))

    def batches_done(self):
        pass

    def field_to_bigquery(self, field):
        name = re.sub('^[^a-zA-Z]', 'x', field[0])
        name = re.sub('[^a-zA-Z0-9_]', '_', name)
        if field[0] in self.pii_columns:
            type = "STRING"
        else:
            type = self.type_map.get(field[1], "STRING")
        return {
            'name': name,
            'type': type,
            'mode': "NULLABLE",
        }

    def upload_schema(self):
        if not self.schema_filename:
            return
        schema = [self.field_to_bigquery(field) for field in self.last_cursor.description]
        self.log.info('Using schema for %s: %s', self.schema_filename, schema)
        with NamedTemporaryFile(delete=True) as tmp_file:
            schema_str = json.dumps(schema, sort_keys=True)
            schema_str = schema_str.encode('utf-8')
            tmp_file.write(schema_str)
            tmp_file.flush()
            upload_to_gcs(self.gcs_conn_id, self.bucket, tmp_file.name, self.schema_filename, mime_type='application/json')

    def close_finished_uploads(self):
        remaining_uploads = []
        for fileobj, proc in self.upload_processes:
            if proc.is_alive():
                remaining_uploads.append((fileobj, proc))
                continue
            else:
                proc.join()
                if proc.exitcode != 0:
                    raise RuntimeError('Upload process did not finish successfully: {}'.format(proc.exitcode))
                fileobj.close()
                os.unlink(fileobj.name)
        self.upload_processes = remaining_uploads

    def await_outstanding_uploads(self):
        for fileobj, proc in self.upload_processes:
            proc.join(self.upload_timeout)
            if proc.exitcode != 0:
                raise RuntimeError('Upload process did not finish successfully: {}'.format(proc.exitcode))
            if os.path.exists(fileobj.name):
                fileobj.close()
                os.unlink(fileobj.name)

    def batches_done(self):
        self.await_outstanding_uploads()
        self.upload_schema()

