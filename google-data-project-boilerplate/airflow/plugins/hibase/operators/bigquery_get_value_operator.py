from airflow.models import BaseOperator
from airflow.providers.google.cloud.hooks.bigquery import BigQueryHook
from airflow.providers.google.cloud.hooks.gcs import GCSHook, _parse_gcs_url
from airflow.utils.decorators import apply_defaults

class BigQueryGetValueOperator(BaseOperator):
    """
    Fetches a value from a BigQuery table and returns the first column of the first row as a python value.

    **Example Result**: ``'foo'``

    **Example**: ::

        get_last_updated = BigQueryGetDataOperator(
            sql='SELECT val FROM dataset.table LIMIT 1',
            gcp_conn_id='airflow-conn-id'
        )
    """

    template_fields = ('sql','error_value')

    @apply_defaults
    def __init__(
        self,
        *,
        sql,
        gcp_conn_id='google_cloud_default',
        error_value=None,
        use_legacy_sql=False,
        **kwargs,
    ) -> None:
        super().__init__(**kwargs)
        self.sql = sql
        self.gcp_conn_id = gcp_conn_id
        self.error_value = error_value
        self.use_legacy_sql = use_legacy_sql

    def execute(self, context) -> list:
        hook = BigQueryHook(
            gcp_conn_id=self.gcp_conn_id,
            use_legacy_sql=self.use_legacy_sql,
        )
        conn = hook.get_conn()
        cursor = conn.cursor()
        try:
            cursor.execute(self.sql)
            (value,) = cursor.fetchone()
            self.log.info('Fetched value: {}'.format(value))
            return value
        except Exception as e:
            if not self.error_value:
                raise
            self.log.warning('Error: {}'.format(e))
            return self.error_value

