locals {
  airflow_gcp_sa_credentials_path = "/secrets/google_application_credentials.json"
  airflow_extra_volumes_and_volume_mounts = {
    "extraVolumes" = [
      {
        "name" = "local-repo"
        "persistentVolumeClaim" = {
          "claimName" = kubernetes_persistent_volume_claim.airflow_local_pvc.metadata[0].name
        }
      },
      {
        "name" = "local-gcp-sa"
        "secret" = {
          "secretName" = kubernetes_secret.airflow_local_gcp_sa.metadata[0].name
        }
      },
      {
        "name" = "variables"
        "secret" = {
          "secretName" = kubernetes_secret.airflow_variables.metadata[0].name
        }
      },
      {
        "name" = "connections"
        "secret" = {
          "secretName" = kubernetes_secret.airflow_connections.metadata[0].name
        }
      },
    ],
    "extraVolumeMounts" = [
      {
        "name" = "local-repo"
        "mountPath" = "/home/airflow/dags"
        "subPath" = "dags"
      },
      {
        "name" = "local-repo"
        "mountPath" = "/home/airflow/logs"
        "subPath" = "logs"
      },
      {
        "name" = "local-repo"
        "mountPath" = "/home/airflow/plugins"
        "subPath" = "plugins"
      },
      {
        "name" = "local-gcp-sa"
        "mountPath" = local.airflow_gcp_sa_credentials_path
        "subPath" = basename(local.airflow_gcp_sa_credentials_path)
      },
      {
        "name" = "variables"
        "mountPath" = "/home/airflow/${local.airflow_variables_filename}"
        "subPath" = local.airflow_variables_filename
      },
      {
        "name" = "connections"
        "mountPath" = "/home/airflow/${local.airflow_connections_filename}"
        "subPath" = local.airflow_connections_filename
      },
    ]
  }
}

module "airflow" {
  source = "../terraform/modules/kubernetes-airflow"
  providers = {
    kubernetes = kubernetes
    helm = helm
  }

  image_registry = "gcr.io/${data.google_project.current.project_id}"
  image_name = "airflow"
  image_tag = "2.0.1"

  database_host = "postgresql.default.svc.cluster.local"
  database_user = module.postgres.username
  database_password = module.postgres.password
  database_name = module.postgres.database

  values = [
    yamlencode({
      "web" = local.airflow_extra_volumes_and_volume_mounts
      "scheduler" = local.airflow_extra_volumes_and_volume_mounts
      "worker" = local.airflow_extra_volumes_and_volume_mounts
    })
  ]

  extra_env = {
    "GOOGLE_APPLICATION_CREDENTIALS" = local.airflow_gcp_sa_credentials_path
    "AIRFLOW__SECRETS__BACKEND" = "airflow.secrets.local_filesystem.LocalFilesystemBackend"
    "AIRFLOW__SECRETS__BACKEND_KWARGS" = jsonencode({
      "variables_file_path" = "/home/airflow/${local.airflow_variables_filename}"
      "connections_file_path" = "/home/airflow/${local.airflow_connections_filename}"
    })
    "AIRFLOW_MIN_FILE_PROCESS_INTERVAL" = "0"
    "AIRFLOW_DAG_DIR_LIST_INTERVAL" = "10"
    "AIRFLOW__WEBSERVER__WORKERS" = "2"
    "AIRFLOW_VAR_ENV" = "dev"
    "AIRFLOW_CONN_POSTGRESQL_KEYBASE" = module.postgres.cluster_internal_superuser_uri
    "AIRFLOW_CONN_GOOGLE_CLOUD_PLATFORM" = "google-cloud-platform://?extra__google_cloud_platform__project=${data.google_project.current.project_id}"
    "AIRFLOW_CONN_GOOGLE_CLOUD_DEFAULT" = "google-cloud-platform://?extra__google_cloud_platform__project=${data.google_project.current.project_id}"
    "AIRFLOW_CONN_BIGQUERY_DEFAULT" = "google-cloud-platform://?extra__google_cloud_platform__project=${data.google_project.current.project_id}"
    "AIRFLOW__SMTP__SMTP_HOST" = ""
    "AIRFLOW__SMTP__SMTP_PORT" = ""
    "AIRFLOW__SMTP__SMTP_USER" = ""
    "AIRFLOW__SMTP__SMTP_MAIL_FROM" = ""
    "AIRFLOW__SMTP__SMTP_PASSWORD" = data.google_secret_manager_secret_version.smtp_password.secret_data
    "AIRFLOW__SMTP__STARTTLS" = "True"
    "AIRFLOW__SMTP__SMTP_SSL" = "False"
  }
}

