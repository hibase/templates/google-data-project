locals {
  airflow_variables_filename = "variables.json"
  airflow_connections_filename = "connections.json"
  secrets_manager_connections = [
    # Add a connection to import here
  ]
}

data google_secret_manager_secret_version "airflow_connections" {
  for_each = toset(local.secrets_manager_connections)
  secret = each.value
}

resource kubernetes_secret "airflow_variables" {
  metadata {
    name = "airflow-vars"
  }

  data = {
    "${local.airflow_variables_filename}" = jsonencode({
      "bigquery_storage_bucket" = ""
      "hello-world" = "Hello from dev!"
    })
  }
}

resource kubernetes_secret "airflow_connections" {
  metadata {
    name = "airflow-conns"
  }

  data = {
    "${local.airflow_connections_filename}" = jsonencode({
      for key in local.secrets_manager_connections:
        replace(key, "af-conn-", "") => data.google_secret_manager_secret_version.airflow_connections[key].secret_data
    })
  }
}


