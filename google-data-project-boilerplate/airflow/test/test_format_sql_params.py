import unittest
import airflow
from hibase.format_sql_params import format_sql_params

class FormatSQLParamsTest(unittest.TestCase):
    def test_format_sql_params(self):
        sql, params = format_sql_params("SELECT $value FROM $$table WHERE $value = 'bar'", table='foo', value='bar')
        self.assertEqual("SELECT ? FROM foo WHERE ? = 'bar'", sql)
        self.assertEqual(('bar', 'bar'), params)

    def test_format_optional_params(self):
        query = """
        SELECT * FROM $$table
        WHERE 1=1
        [[AND $$column = $value]]
        """
        sql, params = format_sql_params(query, table='table_name', column='column_name', value='bar')
        expected_sql = """
        SELECT * FROM table_name
        WHERE 1=1
        AND column_name = ?
        """
        self.assertEqual(expected_sql, sql)
        self.assertEqual(('bar',), params)
        # omitting a param, omits the optional section
        sql, params = format_sql_params(query, table='table_name', column='column_name')
        expected_sql = """
        SELECT * FROM table_name
        WHERE 1=1
        
        """
        self.assertEqual(expected_sql, sql)
        self.assertEqual(tuple(), params)
        # a None param, does not omit the optional section
        sql, params = format_sql_params(query, table='table_name', column='column_name', value=None)
        expected_sql = """
        SELECT * FROM table_name
        WHERE 1=1
        AND column_name = ?
        """
        self.assertEqual(expected_sql, sql)
        self.assertEqual((None,), params)

    def test_format_mixed_params_with_optional(self):
        query = """
        SELECT
            t.*,
            YEAR(t.Birthday) AS Birthyear,
            t.Email AS pii_ref
        FROM dbo.Dim_Customer t
        WHERE t.LastUpdated > CONVERT(DATETIME, $last_modified)
        [[AND t.$$offset_column > $offset_value]]
        [[ORDER BY t.$$offset_column ASC]]
        """
        sql, params = format_sql_params(query, last_modified='1899-01-01', offset_column='CustomerId', offset_value=547772)
        expected_sql = """
        SELECT
            t.*,
            YEAR(t.Birthday) AS Birthyear,
            t.Email AS pii_ref
        FROM dbo.Dim_Customer t
        WHERE t.LastUpdated > CONVERT(DATETIME, ?)
        AND t.CustomerId > ?
        ORDER BY t.CustomerId ASC
        """
        self.assertEqual(expected_sql, sql)
        # Note the params coming out in order
        self.assertEqual(('1899-01-01', 547772), params)

if __name__ == '__main__':
    unittest.main()


