variable "image_registry" {
  type        = string
  default     = ""
  description = "The Airflow image repository to pull the image from"
}

variable "image_tag" {
  type        = string
  default     = "latest"
  description = "The Airflow image tag"
}

variable "image_name" {
  type        = string
  default     = ""
  description = "The Airflow image repository to pull the image from"
}

variable "google_sourcerepo_name" {
  type        = string
  description = "The name of the google_sourcerepo"
}

variable "google_sourcerepo_url" {
  type        = string
  description = "The repository_url of the google_sourcerepo"
}

variable "namespace" {
  type        = string
  default     = "default"
  description = "The namespace to deploy the airflow chart."
}

variable "extra_env" {
  type        = map(string)
  default     = {}
  description = "Extra environment variables to apply to web, scheduler, worker"
}

variable "extra_volumes" {
  type        = list(any)
  default     = []
}

variable "extra_volume_mounts" {
  type        = list(any)
  default     = []
}

variable "values" {
  type        = list(string)
  default     = []
}

variable "variables" {
  type        = map(string)
  default     = {}
}

variable "connections" {
  type        = map(string)
  default     = {}
}

variable "database_host" {
  type = string
}

variable "database_user" {
  type = string
}

variable "database_password" {
  type = string
}

variable "database_name" {
  type = string
}
