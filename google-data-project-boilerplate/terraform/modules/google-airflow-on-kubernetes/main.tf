locals {
  git_clone_sourcerepo_serviceaccount = "airflow-sourcerepo-${random_id.airflow.hex}"
  logs_serviceaccount = "airflow-logs-${random_id.airflow.hex}"
  extra_env = merge({}, var.extra_env)
}

data google_project "current" {}

module "airflow" {
  source = "../kubernetes-airflow"
  providers = {
    kubernetes = kubernetes
    helm = helm
  }

  image_registry = var.image_registry
  image_name = var.image_name
  image_tag = var.image_tag

  extra_env = local.extra_env
  extra_volumes = var.extra_volumes
  extra_volume_mounts = var.extra_volume_mounts

  database_host = var.database_host
  database_user = var.database_user
  database_password = var.database_password
  database_name = var.database_name

  values = flatten([
    yamlencode({
      "git" = {
        "enabled" = true
        "pull" = {
          # https://cloud.google.com/sdk/docs/downloads-docker
          "image" = {
            "registry" = "gcr.io/google.com/cloudsdktool"
            "name" = "cloud-sdk"
            "tag" = "latest"
          }
          "command" = ["/bin/sh"]
          "args" = [
            "-c",
            file("${path.module}/git-pull.sh")
          ]
          "extraVolumes" = [
            {
              "name" = "gcp-sa"
              "secret" = {
                "secretName" = kubernetes_secret.git_clone_sourcerepo.metadata[0].name
              }
            }
          ]
          "extraVolumeMounts" = [
            {
              "name" = "gcp-sa"
              "mountPath" = "/secrets/google_application_credentials.json"
              "subPath" = "google_application_credentials.json"
            }
          ]
        }
        "clone" = {
          # https://cloud.google.com/sdk/docs/downloads-docker
          "image" = {
            "registry" = "gcr.io/google.com/cloudsdktool"
            "name" = "cloud-sdk"
            "tag" = "latest"
          }
          "command" = ["/bin/sh"]
          "args" = [
            "-c",
            templatefile("${path.module}/git-clone.sh", {
              project = data.google_project.current.project_id
              repository_name = var.google_sourcerepo_name
            })
          ]
          "extraVolumes" = [
            {
              "name" = "gcp-sa"
              "secret" = {
                "secretName" = kubernetes_secret.git_clone_sourcerepo.metadata[0].name
              }
            }
          ]
          "extraVolumeMounts" = [
            {
              "name" = "gcp-sa"
              "mountPath" = "/secrets/google_application_credentials.json"
              "subPath" = "google_application_credentials.json"
            }
          ]
        }
      }
    }),
    var.values,
  ])
}

resource random_id "airflow" {
  byte_length = 4
}

resource google_service_account "git_clone_sourcerepo" {
  account_id = local.git_clone_sourcerepo_serviceaccount
  display_name = "Airflow git-clone SA"
  description = "Service account allowed to clone the Airflow source repository"
}

resource google_service_account_key "git_clone_sourcerepo" {
  service_account_id = google_service_account.git_clone_sourcerepo.name
}

resource google_sourcerepo_repository_iam_member "git_clone_sourcerepo" {
  project = data.google_project.current.project_id
  repository = var.google_sourcerepo_name
  role = "roles/viewer"
  member = "serviceAccount:${google_service_account.git_clone_sourcerepo.email}"
}

resource kubernetes_secret "git_clone_sourcerepo" {
  provider = kubernetes

  metadata {
    name = local.git_clone_sourcerepo_serviceaccount
    namespace = var.namespace
  }

  data = {
    "GOOGLE_APPLICATION_CREDENTIALS" = "/secrets/google_application_credentials.json"
    "google_application_credentials.json" = base64decode(google_service_account_key.git_clone_sourcerepo.private_key)
  }
}

