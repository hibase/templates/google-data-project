#!/bin/sh
set -e
gcloud auth activate-service-account --key-file=/secrets/google_application_credentials.json
if test "$(ls -A .)" = ""; then
  rm -rf .* * 2>/dev/null || true
fi
gcloud source repos --project=${project} clone ${repository_name} .

