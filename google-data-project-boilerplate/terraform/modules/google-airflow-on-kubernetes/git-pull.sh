#!/bin/sh
set -e
gcloud auth activate-service-account --key-file=/secrets/google_application_credentials.json
set +e
git config --global user.email "root@serviceaccount.local"
git config --global user.name "Local Serviceaccount"
while true; do
  date
  git fetch --all
  git reset --hard origin/master
  sleep 60
done
