variable name_prefix {
  description = "The prefix name for VPC resources. (required)"
  type = string
}

variable project {
  description = "The project to operate in."
  default = ""
  type = string
}

variable region {
  description = "The region to operate in."
  type = string
}

variable cidr_block {
  description = "The IP address space of the VPC. Note: do not use a prefix higher than /27 (default: 10.0.0.0/16)"
  type = string
  default = "10.0.0.0/16"
}

variable subnetwork_division {
  description = "The difference between your VPC network and subnetwork mask. A subnetwork_division of 4 for a /16 VPC network results in /20 subnetworks. (default: 4)"
  type = number
  default = 4
}

variable secondary_cidr_block {
  description = "The IP address space of the VPC's secondary address range. Note: do not use a prefix higher than /27. (default: 10.1.0.0/16)"
  type = string
  default = "10.1.0.0/16"
}

variable secondary_subnetwork_division {
  description = "The difference between your secondary VPC network and subnetwork mask. A subnetwork_division of 4 for a /16 VPC network results in /20 subnetworks. (default: 4)"
  type = number
  default = 4
}

variable secondary_ip_ranges {
  description = "List of names of the seconary VPC address range names. (default: [])"
  type = list(string)
  default = []
}

