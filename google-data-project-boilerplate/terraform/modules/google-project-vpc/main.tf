terraform {
  required_version = ">= 0.12"
}

locals {
  project = var.project != "" ? var.project : data.google_project.current.project_id
  secondary_ip_cidr = [
    for i, name in var.secondary_ip_ranges:
      cidrsubnet(var.secondary_cidr_block, var.secondary_subnetwork_division, i)
  ]
}

data google_project "current" {}

resource google_compute_network "this" {
  project = var.project
  name    = "${var.name_prefix}-network"

  auto_create_subnetworks = "false"
  routing_mode = "REGIONAL"
}

resource google_compute_router "this" {
  project = var.project
  name = "${var.name_prefix}-router"

  region  = var.region
  network = google_compute_network.this.self_link
}

###
# Public subnetwork behind NAT
#

resource google_compute_subnetwork "public" {
  project = var.project
  name = "${var.name_prefix}-public"

  region  = var.region
  network = google_compute_network.this.self_link

  private_ip_google_access = true
  ip_cidr_range            = cidrsubnet(var.cidr_block, var.subnetwork_division, 0)

  dynamic "secondary_ip_range" {
    for_each = var.secondary_ip_ranges
    content {
      range_name   = secondary_ip_range.value
      ip_cidr_range = local.secondary_ip_cidr[secondary_ip_range.key]
    }
  }
}

resource google_compute_router_nat "public" {
  project = var.project
  name = "${var.name_prefix}-nat"

  region  = var.region
  router  = google_compute_router.this.name

  nat_ip_allocate_option = "AUTO_ONLY"

  # "Manually" define the subnetworks for which the NAT is used, so that we can exclude the public subnetwork
  source_subnetwork_ip_ranges_to_nat = "LIST_OF_SUBNETWORKS"

  subnetwork {
    name                    = google_compute_subnetwork.public.self_link
    source_ip_ranges_to_nat = ["ALL_IP_RANGES"]
  }
}

