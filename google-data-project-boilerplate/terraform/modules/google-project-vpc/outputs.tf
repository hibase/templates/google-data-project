output "network" {
  description = "A reference (self_link) to the VPC network"
  value       = google_compute_network.this.self_link
}

output "region" {
  value       = google_compute_subnetwork.public.region
}

output "public_subnetwork" {
  description = "A reference (self_link) to the public subnetwork"
  value       = google_compute_subnetwork.public.self_link
}

output "public_subnetwork_name" {
  description = "Name of the public subnetwork"
  value       = google_compute_subnetwork.public.name
}

output "public_subnetwork_cidr_block" {
  value = google_compute_subnetwork.public.ip_cidr_range
}

output "public_subnetwork_gateway" {
  value = google_compute_subnetwork.public.gateway_address
}

output "public_subnetwork_secondary_cidr_block" {
  value = google_compute_subnetwork.public.secondary_ip_range[0].ip_cidr_range
}

output "public_subnetwork_secondary_range_name" {
  value = google_compute_subnetwork.public.secondary_ip_range[0].range_name
}

output "secondary_ip_cidr_by_name" {
  value = zipmap(var.secondary_ip_ranges, local.secondary_ip_cidr)
}

