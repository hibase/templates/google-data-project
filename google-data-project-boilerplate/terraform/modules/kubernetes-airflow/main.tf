locals {
  auth_username = "admin-${random_id.auth_username_suffix.hex}"
  auth_password = random_password.auth_password.result
  auth_fernet_key = base64encode(substr(random_password.fernet_key.result, 0, 32))
  extra_env_vars = [for key, value in var.extra_env: {name = key, value = value}]
  # airflow_components = ["web", "scheduler", "worker"]
  # airflow_component_config = {
  #   for component in local.airflow_components: component => {
  #     "image" = {
  #       "registry" = var.image_registry
  #       "repository" = replace(var.image_repository, "{{component}}", component)
  #       "tag" = var.image_tag
  #     }
  #     "extraVolumeMounts" = var.extra_volume_mounts
  #     "extraVolumes" = var.extra_volumes
  #   }
  # }
}

resource helm_release "this" {
  chart      = "${path.module}/airflow-chart"
  namespace  = "default"
  name       = "airflow"
  # version    = "0.18.6"
  # repository = "https://helm.astronomer.io"
  lint       = false
  timeout    = 1200

  values = flatten([
    var.values,
    yamlencode({
      "airflow" = {
        "image" = {
          "registry" = var.image_registry
          "name" = var.image_name
          "tag" = var.image_tag
        }
        "extraEnv" = local.extra_env_vars
        "extraVolumes" = var.extra_volumes
        "extraVolumeMounts" = var.extra_volume_mounts
      }
    })
  ])

  set_sensitive {
    name = "airflow.database.host"
    value = var.database_host
  }

  set_sensitive {
    name = "airflow.database.user"
    value = var.database_user
  }

  set_sensitive {
    name = "airflow.database.password"
    value = var.database_password
  }

  set {
    name = "airflow.database.name"
    value = var.database_name
  }

  set_sensitive {
    name = "airflow.fernetKey"
    value = local.auth_fernet_key
  }

  set_sensitive {
    name = "airflow.secretKey"
    value = random_password.secret_key.result
  }

}

resource random_password "auth_password" {
  length = 64
}

resource random_id "auth_username_suffix" {
  byte_length = 4
}

resource random_password "fernet_key" {
  length = 128
}

resource random_password "secret_key" {
  length = 128
}
