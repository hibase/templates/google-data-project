kind: Deployment
apiVersion: apps/v1
metadata:
  name: {{ .Release.Name }}-scheduler
  namespace: {{ .Release.Namespace }}
  {{ include "metadata.labels" . | nindent 2 }}
    component: scheduler
spec:
  replicas: {{ .Values.scheduler.replicas }}
  strategy:
    type: Recreate
  selector:
    matchLabels:
      app: airflow
      component: scheduler
      release: {{ .Release.Name }}
  template:
    metadata:
      labels:
        app: airflow
        component: scheduler
        release: {{ .Release.Name }}
        {{ include "common.labels" .Values.labels | nindent 8 }}
      annotations:
        checksum/airflow-secret-env: {{ include (print $.Template.BasePath "/secrets/env.yaml") . | sha256sum }}
        checksum/airflow-config: {{ include (print $.Template.BasePath "/configmap.yaml") . | sha256sum }}
    spec:
      restartPolicy: Always
      terminationGracePeriodSeconds: 10
      serviceAccountName: {{ .Release.Name }}-serviceaccount
      securityContext:
        runAsUser: {{ .Values.uid }}
        fsGroup: {{ .Values.gid }}
      {{- include "spec.nodeSelector" . | indent 6 }}
      {{- include "spec.affinity" . | indent 6 }}
      {{- include "spec.tolerations" (concat .Values.tolerations .Values.web.tolerations) | indent 6 }}
      initContainers:
      {{- include "containers.git-clone" . | trim | nindent 6 }}
      {{- include "containers.migrations" . | trim | nindent 6 }}
      containers:
      {{- include "containers.git-pull" . | trim | nindent 6 }}
      - name: scheduler
        image: "{{- include "image" .Values.airflow.image }}"
        imagePullPolicy: {{ .Values.airflow.image.pullPolicy }}
        args: ["airflow", "scheduler"]
        envFrom:
        - secretRef:
            name: {{ .Release.Name }}-env
        env:
        - name: _HELM
          value: _HELM
        {{- if .Values.airflow.extraEnv }}
        {{ toYaml .Values.airflow.extraEnv | nindent 8 }}
        {{- end }}
        {{- if .Values.scheduler.extraEnv }}
        {{ toYaml .Values.scheduler.extraEnv | nindent 8 }}
        {{- end }}
        # If the scheduler stops heartbeating for 5 minutes (10*30s) kill the
        # scheduler and let Kubernetes restart it.
        # Use the same for readiness and liveness.
        {{- range tuple "readinessProbe" "livenessProbe" }}
        {{ . }}:
          initialDelaySeconds: {{ $.Values.scheduler.livenessProbe.initialDelaySeconds }}
          timeoutSeconds: {{ $.Values.scheduler.livenessProbe.timeoutSeconds }}
          failureThreshold: {{ $.Values.scheduler.livenessProbe.failureThreshold }}
          periodSeconds: {{ $.Values.scheduler.livenessProbe.periodSeconds }}
          exec:
            command:
            - /usr/bin/env
            - AIRFLOW__CORE__LOGGING_LEVEL=ERROR
            - python3
            - -Wignore
            - -c
            - |
              from airflow.jobs.scheduler_job import SchedulerJob
              from airflow.utils.db import create_session
              from airflow.utils.net import get_hostname
              import sys

              with create_session() as session:
                job = session.query(SchedulerJob).filter_by(hostname=get_hostname()).order_by(SchedulerJob.latest_heartbeat.desc()).limit(1).first()
              sys.exit(0 if job.is_alive() else 1)
        {{- end }}
        resources:
          {{ toYaml .Values.scheduler.resources | nindent 10 }}
        volumeMounts:
        {{- if .Values.git.enabled }}
        - name: git-sync
          mountPath: /home/airflow/dags
          subPath: dags
        - name: git-sync
          mountPath: /home/airflow/plugins
          subPath: plugins
        {{- end }}
        - name: config
          mountPath: /home/airflow/airflow.cfg
          subPath: airflow.cfg
          readOnly: true
        - name: config
          mountPath: /home/airflow/airflow_local_settings.py
          subPath: airflow_local_settings.py
          readOnly: true
        - name: config
          subPath: pod_template_file.yaml
          mountPath: /home/airflow/pod_template_file.yaml
          readOnly: true
      {{- if .Values.airflow.extraVolumeMounts }}
        {{ toYaml .Values.airflow.extraVolumeMounts | nindent 8 }}
      {{- end }}
      {{- if .Values.scheduler.extraVolumeMounts }}
        {{ toYaml .Values.scheduler.extraVolumeMounts | nindent 8 }}
      {{- end }}
      volumes:
      {{- if .Values.git.enabled }}
      - name: git-sync
        emptyDir: {}
      {{- end }}
      - name: config
        configMap:
          name: {{ .Release.Name }}-config
      {{- if .Values.airflow.extraVolumes }}
      {{ toYaml .Values.airflow.extraVolumes | nindent 6 }}
      {{- end }}
      {{- if .Values.scheduler.extraVolumes }}
      {{ toYaml .Values.scheduler.extraVolumes | nindent 6 }}
      {{- end }}
      {{- if (and .Values.git.enabled .Values.git.clone.extraVolumes) }}
      {{ toYaml .Values.git.clone.extraVolumes | nindent 6 }}
      {{- end }}

