variable "database_host" {
  type = string
}

variable "database_user" {
  type = string
}

variable "database_password" {
  type = string
}

variable "database_name" {
  type = string
}

variable "values" {
  type = list(string)
  default = []
}

variable "extra_env" {
  type = map(string)
  default = {}
}

variable "extra_volumes" {
  type = list(any)
  default = []
}

variable "extra_volume_mounts" {
  type = list(any)
  default = []
}

variable "image_registry" {
  type = string
}

variable "image_name" {
  type = string
}

variable "image_tag" {
  type = string
}

