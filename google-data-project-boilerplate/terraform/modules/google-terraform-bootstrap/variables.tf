variable "project_id" {
  type        = string
  description = "The project ID to work with."
}

variable "bucket_location" {
  type        = string
  description = "The bucket location to create the bucket in."
}

variable "service_account_id" {
  type        = string
  default     = "project-terraform"
  description = "The service account id."
}

variable "existing_service_account_email" {
  type        = string
  default     = ""
  description = "An existing service account to grant administrative privileges for this terraform project."
}

variable "service_account_name" {
  type        = string
  default     = "Terraform Service Account"
  description = "The service accout display name."
}

variable "allowed_impersonation_iam_members" {
  type        = list(string)
  default     = []
  description = "List of iam member entries, that are allowed to impersonate this terraform service account. (e.g. group:devops@domain.com)"
}

variable "terraform_is_project_owner" {
  type        = bool
  default     = false
  description = "Grant project owner rights to the terraform service account."
}
