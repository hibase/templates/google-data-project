locals {
  bucket_name = "${substr(var.project_id, 0, 50)}-tfstate-${random_id.suffix.hex}"
  create_service_account = (var.existing_service_account_email != "" ? false : true)
  existing_service_account_match = (
    local.create_service_account ?
      {} : regex("^(?P<account_id>[^@]+)@(?P<project_id>[-a-z0-9\\.]{1,63})\\.iam\\.gserviceaccount\\.com", var.existing_service_account_email)
  )
  service_account_id = (
    local.create_service_account ?
      google_service_account.this[0].name : data.google_service_account.this[0].name
  )
  service_account_email = (
    local.create_service_account ?
      google_service_account.this[0].email : data.google_service_account.this[0].email
  )
}

resource random_id "suffix" {
  byte_length = 2
}

data google_service_account "this" {
  count      = local.create_service_account ? 0 : 1
  project    = local.existing_service_account_match["project_id"]
  account_id = local.existing_service_account_match["account_id"]
}

resource google_service_account "this" {
  count        = local.create_service_account ? 1 : 0
  project      = var.project_id
  account_id   = var.service_account_id
  display_name = var.service_account_name
}

resource google_service_account_iam_member "this" {
  for_each = local.create_service_account ? toset(var.allowed_impersonation_iam_members) : toset([])

  service_account_id = local.service_account_id
  role               = "roles/iam.serviceAccountTokenCreator"
  member             = each.value
}

resource google_storage_bucket "this" {
  project                     = var.project_id
  name                        = local.bucket_name
  location                    = var.bucket_location
  uniform_bucket_level_access = true
  force_destroy               = false

  versioning {
    enabled = true
  }
}

resource google_storage_bucket_iam_binding "this" {
  bucket = google_storage_bucket.this.name
  role     = "roles/storage.admin"
  members  = toset(flatten([var.allowed_impersonation_iam_members, "serviceAccount:${local.service_account_email}"]))
}

resource google_project_iam_member "terraform_project_owner" {
  count = local.create_service_account ? (var.terraform_is_project_owner ? 1 : 0) : 0

  project = var.project_id
  role    = "roles/owner"
  member  = "serviceAccount:${google_service_account.this[0].email}"
}
