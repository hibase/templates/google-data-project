output "project_id" {
  value = var.project_id
}

output "bucket_name" {
  value = google_storage_bucket.this.name
}

output "service_account_email" {
  value = local.service_account_email
}

