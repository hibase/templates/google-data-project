output registry_base {
  value = "gcr.io/${data.google_project.this.project_id}"
}

output service_account_json {
  value     = var.create_service_account ? base64decode(google_service_account_key.this["one"].private_key) : ""
  sensitive = true
}

