variable "location" {
  type        = string
  default     = ""
  description = "The GCR bucket location"
}

variable "create_service_account" {
  type        = bool
  default     = false
  description = "Create a dedicated service account to access this registry"
}

variable "service_account_id" {
  type        = string
  default     = "project-gcr"
  description = "The service account id."
}

variable "service_account_name" {
  type        = string
  default     = "Container Registry Service Account"
  description = "The service accout display name."
}

variable "storage_object_viewers" {
  type        = list(string)
  default     = []
  description = "List of iam members allowed to pull images"
}

variable "storage_admins" {
  type        = list(string)
  default     = []
  description = "List of iam members allowed to push and pull images"
}

