locals {
  bucket_name = join("", [
    var.location != "" ? "${lower(var.location)}." : "",
    "artifacts.${data.google_project.this.project_id}.appspot.com"
  ])
}

data google_project "this" {}

resource google_container_registry "this" {
  location = var.location
}

resource google_service_account "this" {
  for_each = toset(var.create_service_account ? ["one"] : [])
  account_id   = var.service_account_id
  display_name = var.service_account_name
}

resource google_service_account_key "this" {
  for_each = toset(var.create_service_account ? ["one"] : [])
  service_account_id = google_service_account.this["one"].name
}

resource google_storage_bucket_iam_member "storage_admin_sa" {
  for_each = toset(var.create_service_account ? ["one"] : [])
  bucket = local.bucket_name
  role   = "roles/storage.admin"
  member = "serviceAccount:${google_service_account.this["one"].email}"
  depends_on = [google_container_registry.this]
}

resource google_storage_bucket_iam_member "viewers" {
  count  = length(var.storage_object_viewers)
  bucket = local.bucket_name
  role   = "roles/storage.objectViewer"
  member = element(var.storage_object_viewers, count.index)
  depends_on = [google_container_registry.this]
}

resource google_storage_bucket_iam_member "admins" {
  count  = length(var.storage_admins)
  bucket = local.bucket_name
  role   = "roles/storage.admin"
  member = element(var.storage_admins, count.index)
  depends_on = [google_container_registry.this]
}
