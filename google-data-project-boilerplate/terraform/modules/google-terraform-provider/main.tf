locals {
  org_id                          = var.org_id != "" ? var.org_id : data.google_project.current.org_id
  billing_account_id              = var.billing_account_id != "" ? var.billing_account_id : data.google_project.current.billing_account
  provider_tf                     = templatefile("${path.module}/provider.tf.template", {
    default_region                  = var.default_region
    organization_id                 = local.org_id
    billing_account_id              = local.billing_account_id
    bucket_name                     = var.terraform_bucket_name
    project_id                      = data.google_project.current.project_id
    terraform_service_account_email = var.terraform_service_account_email
  })
}

data google_project "current" {
  project_id = var.terraform_project_id
}

resource local_file "provider_tf" {
  count = var.directory != "" ? 1 : 0

  content  = local.provider_tf
  filename = "${var.directory}/${var.filename}"
}

