variable "terraform_bucket_name" {
  type        = string
  description = "The terraform bucket name."
}

variable "terraform_service_account_email" {
  type        = string
  description = "The terraform service account email that has Owner level access to the project."
}

variable "terraform_project_id" {
  type        = string
  description = "The Project id prefix [a-z][a-z0-9-]."
}

variable "org_id" {
  type        = string
  default     = ""
  description = "The GCP organisation id."
}

variable "billing_account_id" {
  type        = string
  default     = ""
  description = "The GCP billing account id."
}

variable "default_region" {
  type        = string
  default     = ""
  description = "The default region for the google provider"
}

variable "directory" {
  type        = string
  default     = ""
  description = "Where to write the provider.tf file."
}

variable "filename" {
  type        = string
  default     = "provider.tf"
  description = "The filename to write."
}

