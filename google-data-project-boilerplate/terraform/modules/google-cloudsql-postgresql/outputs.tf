output "connection_uri" {
  value = "postgresql://${google_sql_user.this.name}:${urlencode(random_password.user.result)}@${google_sql_database_instance.this.first_ip_address}/${local.database}"
  sensitive = true
}

output "username" {
  value = google_sql_user.this.name
  sensitive = true
}

output "password" {
  value = random_password.user.result
}

output "host" {
  value = google_sql_database_instance.this.first_ip_address
}

output "database" {
  value = local.database
}

output "private_ip_address_name" {
  value = google_compute_global_address.private_ip_address.name
}

