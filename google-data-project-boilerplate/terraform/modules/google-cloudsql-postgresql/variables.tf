variable "name" {
  type = string
}

variable "region" {
  type = string
}

variable "database_name" {
  type = string
  default = ""
}

variable "deletion_protection" {
  type = bool
  default = true
}

variable "tier" {
  type = string
  default = "db-f1-micro"
}

variable "disk_type" {
  type = string
  default = "PD_SSD"
}

variable "publicly_accessible" {
  type = bool
  default = false
}

variable "network" {
  type = string
  default = ""
}

variable "authorized_networks" {
  type = list(any)
  default = []
}

variable "availability_type" {
  type = string
  default = "ZONAL"
}
