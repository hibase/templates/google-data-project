locals {
  instance_name = "${var.name}-${random_id.this.hex}"
  database = var.database_name == "" ? random_pet.database_name.id : var.database_name
}

resource random_id "this" {
  byte_length = 3
}

resource google_compute_global_address "private_ip_address" {
  provider = google-beta

  name          = local.instance_name
  purpose       = "VPC_PEERING"
  address_type  = "INTERNAL"
  prefix_length = 16
  network       = var.network
}

resource google_sql_database_instance "this" {
  name = local.instance_name
  database_version = "POSTGRES_11"
  region = var.region
  deletion_protection = var.deletion_protection

  settings {
    tier = var.tier
    disk_type = var.disk_type

    ip_configuration {
      ipv4_enabled = var.publicly_accessible
      private_network = var.network

      dynamic "authorized_networks" {
        for_each = var.authorized_networks
        content {
          name = authorized_networks.value.name
          value = authorized_networks.value.cidr
        }
      }
    }

    backup_configuration {
      enabled = true
      point_in_time_recovery_enabled = true
    }
  }
}

resource random_pet "user" {
  length = 2
  separator = "-"
}

resource random_password "user" {
  length = 64
}

resource google_sql_user "this" {
  name = random_pet.user.id
  instance = google_sql_database_instance.this.name
  password = random_password.user.result
}

resource random_pet "database_name" {
  length = 2
  separator = "_"
}

resource google_sql_database "this" {
  name = local.database
  instance = google_sql_database_instance.this.name
}

