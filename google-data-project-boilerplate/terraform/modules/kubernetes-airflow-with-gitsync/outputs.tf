output "username" {
  sensitive = true
  value = local.auth_username
}

output "password" {
  sensitive = true
  value = local.auth_password
}
