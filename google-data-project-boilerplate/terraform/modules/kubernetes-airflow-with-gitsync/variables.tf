# Required module inputs

# Configuration

variable "executor" {
  type        = string
  default     = "KubernetesExecutor"
  description = "The Airflow executor to use"
}

variable "image_registry" {
  type        = string
  default     = ""
  description = "The Airflow image repository to pull the image from"
}

variable "image_tag" {
  type        = string
  default     = "latest"
  description = "The Airflow image tag"
}

variable "image_repository" {
  type        = string
  default     = ""
  description = "The Airflow image repository to pull the image from"
}

variable "extra_env" {
  type        = map(string)
  default     = {}
  description = "Extra environment variables to apply to web, scheduler, worker"
}

variable "extra_volumes" {
  type        = list(any)
  default     = []
}

variable "extra_volume_mounts" {
  type        = list(any)
  default     = []
}

variable "variables" {
  type        = map(string)
  default     = {}
}

variable "connections" {
  type        = map(string)
  default     = {}
}

# Helm

variable "helm_chart_name" {
  type        = string
  default     = "airflow"
  description = "Helm chart name to be installed"
}

variable "helm_chart_version" {
  type        = string
  default     = "7.0.3"
  description = "Version of the Helm chart"
}

variable "helm_release_name" {
  type        = string
  default     = "airflow"
  description = "Helm release name"
}

variable "helm_repo_url" {
  type        = string
  default     = "https://charts.bitnami.com/bitnami"
  description = "Helm repository"
}

# Kubernetes

variable "namespace" {
  type        = string
  default     = "default"
  description = "The Kubernetes namespace in which resources are created"
}

variable "service_account_name" {
  default     = "airflow"
  description = "The Kubernetes service account name"
}

variable "settings" {
  type        = map(any)
  default     = {}
  description = "Additional settings which will be passed to the Helm chart values"
}

variable "values" {
  type        = list(string)
  default     = []
  description = "Additional values yaml contents to pass to the Helm chart"
}

