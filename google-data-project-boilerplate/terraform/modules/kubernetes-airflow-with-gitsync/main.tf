locals {
  auth_username = "admin-${random_id.auth_username_suffix.hex}"
  auth_password = random_password.auth_password.result
  auth_fernet_key = base64encode(substr(random_password.fernet_key.result, 0, 32))
  extra_env_vars = [for key, value in var.extra_env: {name = key, value = value}]
  airflow_components = ["web", "scheduler", "worker"]
  airflow_component_config = {
    for component in local.airflow_components: component => {
      "image" = {
        "registry" = var.image_registry
        "repository" = replace(var.image_repository, "{{component}}", component)
        "tag" = var.image_tag
      }
      "extraVolumeMounts" = var.extra_volume_mounts
      "extraVolumes" = var.extra_volumes
    }
  }
}

resource helm_release "this" {
  chart      = var.helm_chart_name
  namespace  = var.namespace
  name       = var.helm_release_name
  version    = var.helm_chart_version
  repository = var.helm_repo_url
  lint       = false
  timeout    = 1200

  values     = flatten([
    yamlencode(local.airflow_component_config),
    yamlencode({
      "extraEnvVars" = local.extra_env_vars
      "extraVolumes" = var.extra_volumes
    }),
    var.values,
  ])

  set {
    name  = "airflow.baseUrl"
    value = "http://airflow.local"
  }

  set {
    name  = "auth.username"
    value = local.auth_username
  }

  set {
    name  = "auth.password"
    value = local.auth_password
  }

  set {
    name  = "auth.fernetKey"
    value = local.auth_fernet_key
  }

  set {
    name  = "airflow.auth.forcePassword"
    value = "true"
  }

  set {
    name  = "executor"
    value = var.executor
  }

  set {
    name  = "redis.enabled"
    value = var.executor == "CeleryExecutor" ? true : false
  }

  set {
    name  = "externalRedis.host"
    value = ""
  }

  set {
    name  = "externalRedis.password"
    value = ""
  }

  set {
    name  = "externalRedis.username"
    value = ""
  }

  set {
    name  = "externalRedis.port"
    value = ""
  }

  set {
    name  = "rbac.create"
    value = true
  }

  set {
    name  = "serviceAccount.create"
    value = true
  }

  set {
    name  = "metrics.enabled"
    value = true
  }

  set {
    name  = "postgresql.postgresqlPassword"
    value = random_password.postgresql_password.result
  }

  dynamic "set" {
    for_each = var.settings
    content {
      name  = set.key
      value = set.value
    }
  }

}

resource random_password "postgresql_password" {
  length = 64
}

resource random_password "auth_password" {
  length = 64
}

resource random_id "auth_username_suffix" {
  byte_length = 4
}

resource random_password "fernet_key" {
  length = 128
}

