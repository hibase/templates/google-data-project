terraform {
  required_version = ">= 0.12"
}

locals {
  cluster_name = join("-", [var.name, random_id.this.hex])
  version      = var.kubernetes_version != "" ? var.kubernetes_version : data.google_container_engine_versions.location.latest_node_version
  location     = var.location != "" ? var.location : var.region
  node_oauth_scopes = [
    "https://www.googleapis.com/auth/devstorage.read_only",
    "https://www.googleapis.com/auth/logging.write",
    "https://www.googleapis.com/auth/monitoring",
  ]
}

resource random_id "this" {
  byte_length = 4
}

data google_container_engine_versions "location" {
  location = local.location
}

data google_compute_subnetwork "this" {
  self_link = var.subnetwork
}

data google_compute_zones "available" {
  region = data.google_compute_subnetwork.this.region
}

data template_file "kubeconfig" {
  template = file("${path.module}/kubeconfig.tpl")

  vars = {
    cluster_ca_certificate = google_container_cluster.this.master_auth.0.cluster_ca_certificate
    endpoint = google_container_cluster.this.endpoint
    longname = "gke_${google_container_cluster.this.project}_${google_container_cluster.this.location}_${google_container_cluster.this.name}"
  }
}

resource random_shuffle "available_zones" {
  input        = data.google_compute_zones.available.names
  result_count = 3
}

resource google_container_cluster "this" {
  provider           = google-beta
  name               = local.cluster_name
  location           = local.location
  node_locations     = var.region != "" ? sort(random_shuffle.available_zones.result) : []
  min_master_version = local.version
  network            = var.network
  subnetwork         = var.subnetwork

  cluster_autoscaling {
    enabled = var.cluster_autoscaling_enabled

    dynamic "resource_limits" {
      for_each = var.cluster_autoscaling_enabled ? [
        {
          type = "cpu",
          max = var.cluster_autoscaling_max_cpu
        },
        {
          type = "memory"
          max = var.cluster_autoscaling_max_memory
        }
      ] : []
      content {
        resource_type = lookup(resource_limits.value, "type")
        minimum       = 0
        maximum       = lookup(resource_limits.value, "max")
      }
    }

    dynamic "auto_provisioning_defaults" {
      for_each = var.cluster_autoscaling_enabled ? [1] : []
      content {
        oauth_scopes = local.node_oauth_scopes
      }
    }
  }

  lifecycle {
    ignore_changes = [
      node_pool,
      initial_node_count
    ]
  }

  # We can't create a cluster with no node pool defined, but we want to only use
  # separately managed node pools. So we create the smallest possible default
  # node pool and immediately delete it.
  remove_default_node_pool = true
  initial_node_count       = 1

  master_auth {
    # Note: Basic auth is deprecated.
    #       Setting an empty username and password explicitly disables basic auth.
    username = ""
    password = ""
    # Note: Client certificate configuration is deprecated.
    client_certificate_config {
      issue_client_certificate = false
    }
  }

  master_authorized_networks_config {
    dynamic "cidr_blocks" {
      for_each = var.master_trusted_cidr_blocks
      content {
        display_name = cidr_blocks.key
        cidr_block   = cidr_blocks.value
      }
    }
  }

  maintenance_policy {
    daily_maintenance_window {
      start_time = "11:00"
    }
  }

  ip_allocation_policy {
    cluster_secondary_range_name  = var.cluster_secondary_range_name
    services_secondary_range_name = var.services_secondary_range_name
  }

  private_cluster_config {
    enable_private_endpoint = var.enable_private_endpoint
    enable_private_nodes    = var.enable_private_nodes
    master_ipv4_cidr_block  = var.enable_private_nodes ? var.master_ipv4_cidr_block : ""
  }
}

resource google_container_node_pool "pools" {
  count = length(var.node_pools)
  name = lookup(var.node_pools[count.index], "name", "pool-${count.index}")
  cluster = google_container_cluster.this.name
  location = var.location
  version = lookup(var.node_pools[count.index], "auto_upgrade", false) ? "" : local.version

  management {
    auto_upgrade = lookup(var.node_pools[count.index], "auto_upgrade", false)
    auto_repair = true
  }

  autoscaling {
    min_node_count = lookup(var.node_pools[count.index], "min_node_count", 0)
    max_node_count = lookup(var.node_pools[count.index], "max_node_count", 100)
  }

  initial_node_count = lookup(
    var.node_pools[count.index],
    "initial_node_count",
    lookup(var.node_pools[count.index], "min_node_count", 0)
  )

  node_config {
    preemptible = lookup(var.node_pools[count.index], "preemptible", false)
    image_type = lookup(var.node_pools[count.index], "image_type", "COS")
    machine_type = lookup(var.node_pools[count.index], "machine_type", "n1-standard-2")
    disk_size_gb = lookup(var.node_pools[count.index], "disk_size_gb", 100)
    disk_type = lookup(var.node_pools[count.index], "disk_type", "pd-standard")

    oauth_scopes = local.node_oauth_scopes

    labels = {
      "cluster_name" = local.cluster_name
      "node_pool" = lookup(var.node_pools[count.index], "name", "pool-${count.index}")
    }

    dynamic "taint" {
      for_each = lookup(var.node_pools[count.index], "dedicated", "") != "" ? [var.node_pools[count.index]["dedicated"]] : []

      content {
        effect = "NO_SCHEDULE"
        key = "dedicated"
        value = taint.value
      }
    }
  }

  lifecycle {
    ignore_changes = [
      initial_node_count
    ]
  }
}

