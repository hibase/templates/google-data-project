apiVersion: v1
clusters:
- cluster:
    certificate-authority-data: ${cluster_ca_certificate}
    server: https://${endpoint}
  name: ${longname}
contexts:
- context:
    cluster: ${longname}
    user: ${longname}
  name: ${longname}
current-context: ${longname}
kind: Config
preferences: {}
users:
- name: ${longname}
  user:
    auth-provider:
      config:
        cmd-args: config config-helper --format=json
        cmd-path: gcloud
        expiry-key: '{.credential.token_expiry}'
        token-key: '{.credential.access_token}'
      name: gcp

