resource kubernetes_limit_range "default" {
  metadata {
    name = "default-limit-range"
  }
  spec {
    dynamic "limit" {
      for_each = var.default_limit_range
      content {
        type = limit.key
        default = limit.value
      }
    }
  }
}

