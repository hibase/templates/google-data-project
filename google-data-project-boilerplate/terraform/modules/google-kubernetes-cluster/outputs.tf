output cluster_name {
  value = google_container_cluster.this.name
}

output endpoint {
  value = google_container_cluster.this.endpoint
}

output client_certificate {
  value = base64decode(google_container_cluster.this.master_auth.0.client_certificate)
}

output client_key {
  value = base64decode(google_container_cluster.this.master_auth.0.client_key)
}

output cluster_ca_certificate {
  value = base64decode(google_container_cluster.this.master_auth.0.cluster_ca_certificate)
}

output kubeconfig {
  value = data.template_file.kubeconfig.rendered
}

output kubeconfig_context {
  value = "gke_${google_container_cluster.this.project}_${google_container_cluster.this.location}_${google_container_cluster.this.name}"
}

output cluster_services_ipv4_cidr {
  value = google_container_cluster.this.services_ipv4_cidr
}

