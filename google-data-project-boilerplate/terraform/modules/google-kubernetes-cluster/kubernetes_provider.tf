provider "kubernetes" {
  host = "https://${google_container_cluster.this.endpoint}"
  client_certificate = base64decode(google_container_cluster.this.master_auth.0.client_certificate)
  client_key = base64decode(google_container_cluster.this.master_auth.0.client_key)
  cluster_ca_certificate = base64decode(google_container_cluster.this.master_auth.0.cluster_ca_certificate)
}

