variable name {
  type        = string
  description = "The name Kubernetes cluster"
}

variable region {
  default = ""
}

variable location {
  default = ""
}

variable network {
  type = string
  default = ""
}

variable subnetwork {
  type = string
}

variable kubernetes_version {
  default = ""
}

variable enable_private_endpoint {
  default = false
}

variable enable_private_nodes {
  default = true
}

variable master_ipv4_cidr_block {
  default = "172.31.255.240/28"
}

variable master_trusted_cidr_blocks {
  type = map(string)
  default = {}
}

variable node_pools {
  type = list(map(string))
  default = []
}

variable cluster_autoscaling_enabled {
  type = bool
  default = true
}

variable cluster_autoscaling_max_cpu {
  type = number
  default = 10
}

variable cluster_autoscaling_max_memory {
  type = number
  default = 64
}

variable cluster_autoscaling_profile {
  type    = string
  default = "BALANCED"
}

variable issue_client_certificate {
  type = bool
  default = false
}

variable cluster_secondary_range_name {
  type = string
  default = "pods"
}

variable services_secondary_range_name {
  type = string
  default = "services"
}

variable create_loadbalancer_static_ip_address {
  type = bool
  default = true
}

variable default_limit_range {
  type = map(any)
  default = {}
}
