resource google_compute_address "loadbalancer_static_ip" {
  count = var.create_loadbalancer_static_ip_address ? 1 : 0
  name = local.cluster_name
}
