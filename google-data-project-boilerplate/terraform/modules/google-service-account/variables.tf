variable "account_id" {
  type        = string
  description = "The account_id of the service account"
}

variable "display_name" {
  type        = string
  default     = ""
  description = "The service account display name"
}

variable "description" {
  type        = string
  default     = ""
  description = "The service account description"
}

variable "create_key" {
  type        = bool
  default     = false
  description = "Optionally create a key for the servive account"
}

variable "provide_key_in_secrets_manager" {
  type        = bool
  default     = false
  description = "Optionally store the key in Google Secrets Manager"
}

variable "secret_location" {
  type        = string
  default     = "europe-west1"
  description = "The location/region to replicate the Google Secrets Manager secret to"
}
