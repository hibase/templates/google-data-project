output "name" {
  value = google_service_account.this.name
}

output "email" {
  value = google_service_account.this.email
}

output "private_key" {
  sensitive = true
  value = var.create_key ? google_service_account_key.this["one"].private_key : ""
}

output "public_key" {
  sensitive = true
  value = var.create_key ? google_service_account_key.this["one"].public_key : ""
}

output "secret_id" {
  sensitive = true
  value = var.create_key ? (var.provide_key_in_secrets_manager ? google_secret_manager_secret.this["one"].secret_id : "") : ""
}

