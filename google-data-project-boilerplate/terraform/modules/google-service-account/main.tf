resource google_service_account "this" {
  account_id = var.account_id
  display_name = var.display_name
  description = var.description
}

resource google_service_account_key "this" {
  for_each = toset(var.create_key ? ["one"] : [])
  service_account_id = google_service_account.this.name
}

resource google_secret_manager_secret "this" {
  for_each = toset(var.create_key ? (var.provide_key_in_secrets_manager ? ["one"] : []) : [])
  secret_id = "sa-${google_service_account.this.account_id}"

  replication {
    user_managed {
      replicas {
        location = var.secret_location
      }
    }
  }
}

resource google_secret_manager_secret_version "this" {
  for_each = toset(var.create_key ? (var.provide_key_in_secrets_manager ? ["one"] : []) : [])
  secret = google_secret_manager_secret.this["one"].id
  secret_data = base64decode(google_service_account_key.this["one"].private_key)
}

