locals {
  default_priority = 1000
  default_deny_all_rule = {
    effect = "deny"
    target_tags = []
    protocol = "all"
    ports = []
    source_ranges = ["0.0.0.0/0"]
    priority = local.default_priority
  }
  all_rules = flatten([
    for target_rule in var.target_rules: flatten([
      (var.default_deny_all ? [local.default_deny_all_rule] : []),
      [for allow in target_rule.allow: merge(allow, { priority = target_rule.priority, target_tags = target_rule.target_tags, effect = "allow" })],
      [for deny in target_rule.deny: merge(deny, { priority = target_rule.priority, target_tags = target_rule.target_tags, effect = "deny" })],
    ])
  ])
  rules = {
    for i, rule in local.all_rules: i => rule
  }
}

resource random_id "rules" {
  for_each = local.rules
  byte_length = 3
}

resource google_compute_firewall "rules" {
  for_each = local.rules

  name          = "${each.value.effect}-${random_id.rules[each.key].hex}"
  network       = var.network
  source_ranges = each.value.source_ranges
  target_tags   = each.value.target_tags
  priority      = each.value.priority

  dynamic "allow" {
    for_each = each.value.effect == "allow" ? [each.value] : []
    content {
      protocol = allow.value.protocol
      ports = allow.value.ports
    }
  }

  dynamic "deny" {
    for_each = each.value.effect == "deny" ? [each.value] : []
    content {
      protocol = deny.value.protocol
      ports = deny.value.ports
    }
  }

  depends_on = [random_id.rules]
}

