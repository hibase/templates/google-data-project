variable network {
  type        = string
  description = "The VPC network id"
}

variable default_deny_all {
  type        = bool
  default     = true
  description = "Enable/disable default deny all rule"
}

variable target_rules {
  type        = list(
    object({
      priority = number
      target_tags = list(string)
      allow = list(
        object({
          protocol = string
          ports = list(string)
          source_ranges = list(string)
          source_tags = list(string)
        })
      )
      deny = list(
        object({
          protocol = string
          ports = list(string)
          source_ranges = list(string)
          source_tags = list(string)
        })
      )
    })
  )
  default     = []
  description = "List of rules to allow traffic for a list of targets"
}

