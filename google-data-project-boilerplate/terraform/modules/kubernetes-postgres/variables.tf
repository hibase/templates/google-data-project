# Required module inputs

# Configuration

variable "username" {
  type        = string
  default     = ""
  description = "The database username"
}

variable "password" {
  type        = string
  default     = ""
  description = "The database user password"
}

variable "superuser_password" {
  type        = string
  default     = ""
  description = "The password of the superuser"
}

variable "database" {
  type        = string
  default     = ""
  description = "The database name to create"
}

variable "extra_env" {
  type        = map(string)
  default     = {}
  description = "Extra environment variables to apply to web, scheduler, worker"
}

# Helm

variable "helm_chart_name" {
  type        = string
  default     = "postgresql"
  description = "Helm chart name to be installed"
}

variable "helm_chart_version" {
  type        = string
  default     = "10.1.3"
  description = "Version of the Helm chart"
}

variable "helm_release_name" {
  type        = string
  default     = ""
  description = "Helm release name"
}

variable "helm_repo_url" {
  type        = string
  default     = "https://charts.bitnami.com/bitnami"
  description = "Helm repository"
}

# Kubernetes

variable "namespace" {
  type        = string
  default     = "default"
  description = "The Kubernetes namespace in which resources are created"
}

variable "service_account_name" {
  default     = "postgres"
  description = "The Kubernetes service account name"
}

variable "settings" {
  type        = map(any)
  default     = {}
  description = "Additional settings which will be passed to the Helm chart values"
}

variable "values" {
  type        = list(string)
  default     = []
  description = "Additional values yaml contents to pass to the Helm chart"
}

