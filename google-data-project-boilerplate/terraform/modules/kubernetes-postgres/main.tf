locals {
  release_name = var.helm_release_name == "" ? "postgresql" : var.helm_release_name
  database = var.database == "" ? random_pet.database.id : var.database
  username = var.username == "" ? random_pet.username.id : var.username
  password = var.password == "" ? random_password.user.result : var.password
  superuser_password = var.superuser_password == "" ? random_password.postgres.result : var.superuser_password
  extra_env_vars = [for key, value in var.extra_env: {name = key, value = value}]
  cluster_internal_hostname = "${local.release_name}.${var.namespace}.svc.cluster.local"
}

resource helm_release "this" {
  chart      = var.helm_chart_name
  namespace  = var.namespace
  name       = local.release_name
  version    = var.helm_chart_version
  repository = var.helm_repo_url
  lint       = true
  timeout    = 1200
  values     = flatten([
    var.values,
    yamlencode({extraEnv = local.extra_env_vars})
  ])

  set {
    name  = "rbac.create"
    value = true
  }

  set {
    name  = "serviceAccount.create"
    value = true
  }

  set {
    name  = "serviceAccount.name"
    value = var.service_account_name
  }

  set {
    name  = "metrics.enabled"
    value = true
  }

  set {
    name  = "postgresqlUsername"
    value = local.username
  }

  set {
    name  = "postgresqlPassword"
    value = local.password
  }

  set {
    name  = "postgresqlPostgresPassword"
    value = local.superuser_password
  }

  set {
    name  = "postgresqlDatabase"
    value = local.database
  }

  dynamic "set" {
    for_each = var.settings
    content {
      name  = set.key
      value = set.value
    }
  }

}

resource random_id "postgres" {
  byte_length = 3
}

resource random_password "postgres" {
  length = 64
  special = false
}

resource random_password "user" {
  length = 64
  special = false
}

resource random_pet "username" {
  length = 2
  separator = "-"
}

resource random_pet "database" {
  length = 2
  separator = "_"
}

