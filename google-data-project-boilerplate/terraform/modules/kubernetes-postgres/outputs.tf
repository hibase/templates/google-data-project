output "username" {
  sensitive = true
  value = local.username
}

output "password" {
  sensitive = true
  value = local.password
}

output "superuser" {
  sensitive = true
  value = "postgres"
}

output "superuser_password" {
  sensitive = true
  value = local.superuser_password
}

output "database" {
  value = local.database
}

output "cluster_internal_hostname" {
  value = local.cluster_internal_hostname
}

output "cluster_internal_uri" {
  value = "postgresql://${local.username}:${urlencode(local.password)}@${local.cluster_internal_hostname}:5432/${local.database}"
}

output "cluster_internal_superuser_uri" {
  value = "postgresql://postgres:${urlencode(local.superuser_password)}@${local.cluster_internal_hostname}:5432/${local.database}"
}

output "name" {
  value = local.release_name
}

output "port" {
  value = 5432
}
