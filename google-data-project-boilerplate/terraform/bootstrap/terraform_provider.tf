module "terraform_provider" {
  source = "../modules/google-terraform-provider"

  default_region                  = "europe-west1"
  directory                       = "${path.module}/../${data.google_project.current.name}"
  terraform_project_id            = data.google_project.current.project_id
  terraform_service_account_email = module.terraform_project.service_account_email
  terraform_bucket_name           = module.terraform_project.bucket_name
}
