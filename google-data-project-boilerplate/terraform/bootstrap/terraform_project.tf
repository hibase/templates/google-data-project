module "terraform_project" {
  source = "../modules/google-terraform-bootstrap"

  project_id           = data.google_project.current.project_id
  bucket_location      = "EU"
  service_account_id   = "terraform"
  service_account_name = "Terraform Service Account"

  terraform_is_project_owner        = true
  allowed_impersonation_iam_members = [
    # Add allowed members for IAM impersonation
    # "user:email@company.tld"
  ]
}

