terraform {
  required_version = "~>0.14.6"
  required_providers {
    google      = "~>3.41"
    google-beta = "~>3.41"
  }
}

provider "google" {}

provider "google-beta" {}

