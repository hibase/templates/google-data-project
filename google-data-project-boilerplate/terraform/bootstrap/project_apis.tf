locals {
  project_apis = [
    "cloudbilling.googleapis.com",
    "cloudresourcemanager.googleapis.com",
    "cloudkms.googleapis.com",
    "compute.googleapis.com",
    "container.googleapis.com",
    "dns.googleapis.com",
    "domains.googleapis.com",
    "storage-component.googleapis.com",
    "serviceusage.googleapis.com",
    "iamcredentials.googleapis.com",
    "iam.googleapis.com",
    "sourcerepo.googleapis.com",
    "secretmanager.googleapis.com",
    "servicenetworking.googleapis.com",
    "sqladmin.googleapis.com",
    "bigquerydatatransfer.googleapis.com",
  ]
}

resource google_project_service "project_apis" {
  for_each = toset(local.project_apis)
  service = each.value
}
