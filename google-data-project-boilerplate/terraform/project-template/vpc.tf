locals {
  kubernetes_pods_alias_range = "pods"
  kubernetes_services_alias_range = "services"
}

resource google_service_networking_connection "private_vpc_connection" {
  provider = google-beta

  network                 = data.google_compute_network.default.id
  service                 = "servicenetworking.googleapis.com"
  reserved_peering_ranges = [
    module.airflow_postgresql.private_ip_address_name,
  ]
}

