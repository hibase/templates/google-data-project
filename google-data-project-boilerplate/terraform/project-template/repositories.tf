locals {
  repositories = [
    "data-foundation",
  ]
}

resource google_sourcerepo_repository "repositories" {
  for_each = toset(local.repositories)
  name = each.value
}

