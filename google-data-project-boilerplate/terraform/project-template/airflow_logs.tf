resource random_id "airflow_logs" {
  byte_length = 3
}

resource google_storage_bucket "airflow_logs" {
  name = "airflow-logs-${random_id.airflow_logs.hex}"
  location = "EU"
  force_destroy = false
}

resource google_storage_bucket_iam_member "airflow_logs" {
  bucket = google_storage_bucket.airflow_logs.name
  role = "roles/storage.admin"
  member = "serviceAccount:${module.airflow_service_account.email}"
}
