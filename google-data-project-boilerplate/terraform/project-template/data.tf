locals {
  region = "europe-west1"
}

data google_project "current" {}

data google_compute_zones "europe_west1" {
  region = local.region
}

data google_compute_network "default" {
  name = ""
}

data google_compute_subnetwork "default_public" {
  name = ""
}

data google_compute_subnetwork "default_private" {
  name = ""
}

# data google_secret_manager_secret_version "smtp_password" {
#   secret = "smtp_password"
# }

