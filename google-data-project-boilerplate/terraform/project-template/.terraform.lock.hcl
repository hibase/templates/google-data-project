# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/hashicorp/google" {
  version     = "3.51.0"
  constraints = "~> 3.47"
  hashes = [
    "h1:xSmUnxWTmQjfwBeWG5rMk5hzJPpqXmbpAOsnrtxZJbU=",
    "zh:378a3aa47b3a9ca096cd7a01a054e2cb1cd8118fbb8a6e4b9cf4cfe196ba9c0d",
    "zh:386706918657af83062fb8fbad0b91939e3b565c286e620e280b87af62dbb497",
    "zh:4b7db10a88cba12c42433e8d04ccabbe68a37cf7b6039d908cd49394c3c37059",
    "zh:64250b8f23e8011c85ac7704594e0bb49bab3d6a628a0832ce53573ac751d29e",
    "zh:7428d6ae1f535628c50e6f1f13d8a70f37fe44332a47d2bcccd2768cab2e6c65",
    "zh:7a1bf9289eef355652f72bfcf92c3e7948b709196e009993a089ad78fdf59ef7",
    "zh:8273cf273f64fad55536a2b666e172fea13840c05eee1528f9ecd9c664d2387e",
    "zh:8be7e42840090bb767ab8f1515d036858a7237d1b2c4aeaa13d6ce6a3b98c2d4",
    "zh:935f9aee07105720e91d9d7b9e946d733087464696fc7594316d3195c2bc3156",
    "zh:e5798365934ab1404f47165a8b0ee358844c86ed73d407c933f5f5510e635b47",
  ]
}

provider "registry.terraform.io/hashicorp/google-beta" {
  version     = "3.51.0"
  constraints = "~> 3.47"
  hashes = [
    "h1:WfwUQQEG90QdOu1wC1x2MVG+nlPV6hcagbpUFaNbroM=",
    "zh:0d2c872ae2485f9b3db5db9d8e6d47ccd22e0b41f5ca0d94610fc29b18aeae8f",
    "zh:4c7c46b04ace002989c4c849581160677128ed892e28f434f721f04e0bc659d5",
    "zh:54e6d1fd59bf8aad0dcf41095cf829c467ff2f8b224cab6d774eabe997195549",
    "zh:588eabb602ba863b7f58e9236d90ab66f7389ece999e7bc2907ea0aacb7c5e72",
    "zh:a08a8b946d1b9f12b87aa5dfaa67f6d6a74b34677842f0005c23b0d847273bb2",
    "zh:ab4ea0d117a30527c31dbbea9f86c4ba639a7bc0e5242cab8582f988708228f2",
    "zh:b6a292830d72f7de8b8c9cda343d0305f09d3f19a7d766bcccebac502c5dd27d",
    "zh:d188ce9907a0aca42fe3fbe8d26d05486afa425dc7c5dd7342470f05d91bd908",
    "zh:dad1ae7fbf689100b632e3c0c56e97e7fc463b39e3e71580b7af9871b136b40b",
    "zh:f95d29886e65c551e8100d1fd889d042f6c1342d3349663b2894151f466175d3",
  ]
}

provider "registry.terraform.io/hashicorp/helm" {
  version = "2.0.1"
  hashes = [
    "h1:tRswDTSMF9ez1y+6KZKSRSOjvHZ0BrQ1+SmbEtfnKm8=",
    "zh:09abaeb5bf484b229b5b6a0f62003bf77501d672913770013c5dfdc6556006fa",
    "zh:0a335b56918fed5316677329ee127d0cd34aee2a9ae7d2a17d68993e26d3d0cc",
    "zh:40dfd1afde160d552c3cc1c88d47e7939aa7758479c2369699fbaf6b78ca41bc",
    "zh:5aaa4ab62b1842684f5180227a7fd1ca9d79bb909e2b226a19bac47e5cdda185",
    "zh:5b1cd59f7eb9b4518b89515bcb0c8494c11c1159a31f08084ed5d359ba3b2883",
    "zh:a4175b9d848addfdbc6ff376ed27e5002468bb9036102ae2b100bdd987b8a2e8",
    "zh:abe34b3cae3629d4bd32c4c6f059648df0b10cbd17a1c2026993613b681da6ca",
    "zh:b21cff4e82b83da84cd93fa69d61d89e1ba2a4b71baa9804c1738bf6ab308118",
    "zh:b2f42452b47cd9d1c9d9fedc3e2ea17ffa2fc89a957ca6549bafbf4f3ec0ba42",
    "zh:d9416219a2abcc5f8d1740cbe2647400ed670137e91713f4053c45fc58602920",
  ]
}

provider "registry.terraform.io/hashicorp/kubernetes" {
  version = "1.13.3"
  hashes = [
    "h1:iV0RArGM0KJ3n0ctwpNzcTkTAnx5G4wV3V3kYkRpJXU=",
    "zh:11fd58df9c297a4f3cab82ce8eb2f54f1be27f78fa23be2273ecd545ab254b91",
    "zh:5b9e6f352c5666d791e2658a1d18bf0990f3ab70c99c916c393a2ee7f385364c",
    "zh:5c94f1350471a5c8e8ee6675874608c506a0bfd3164bdd91b802842723547e2c",
    "zh:5d9c5c44dba9addbb86491339012096e74778bb4ea93b70f12333bffba3d05e6",
    "zh:6336f9cbb0b580f247cebb97fb7d4cc5e7fe9cc734d8d958d84c4ea3f1e24041",
    "zh:bca3b9d4dcbe6f804f5611a83add371dc03b5aa92271f60ebdc2216bfedfab28",
    "zh:cbcdc87a593090f490f7899f4f2d302e0c7023155591fcf65e6fadd69f5452f0",
    "zh:ec2886a1adbfe3c861b2deb9446369111b9c6116701ae73ef372dc7df5bb3c9e",
    "zh:edb5b4172610672bb4d7425511961fda2047b8a00675b99ae6887cd2ece4bda9",
    "zh:ff7ea7743246181ea739643d7751c37041c4016eb6bbc39beb1e3b4e99629112",
  ]
}

provider "registry.terraform.io/hashicorp/random" {
  version = "3.0.0"
  hashes = [
    "h1:yhHJpb4IfQQfuio7qjUXuUFTU/s+ensuEpm23A+VWz0=",
    "zh:0fcb00ff8b87dcac1b0ee10831e47e0203a6c46aafd76cb140ba2bab81f02c6b",
    "zh:123c984c0e04bad910c421028d18aa2ca4af25a153264aef747521f4e7c36a17",
    "zh:287443bc6fd7fa9a4341dec235589293cbcc6e467a042ae225fd5d161e4e68dc",
    "zh:2c1be5596dd3cca4859466885eaedf0345c8e7628503872610629e275d71b0d2",
    "zh:684a2ef6f415287944a3d966c4c8cee82c20e393e096e2f7cdcb4b2528407f6b",
    "zh:7625ccbc6ff17c2d5360ff2af7f9261c3f213765642dcd84e84ae02a3768fd51",
    "zh:9a60811ab9e6a5bfa6352fbb943bb530acb6198282a49373283a8fa3aa2b43fc",
    "zh:c73e0eaeea6c65b1cf5098b101d51a2789b054201ce7986a6d206a9e2dacaefd",
    "zh:e8f9ed41ac83dbe407de9f0206ef1148204a0d51ba240318af801ffb3ee5f578",
    "zh:fbdd0684e62563d3ac33425b0ac9439d543a3942465f4b26582bcfabcb149515",
  ]
}

provider "registry.terraform.io/hashicorp/template" {
  version = "2.2.0"
  hashes = [
    "h1:0wlehNaxBX7GJQnPfQwTNvvAf38Jm0Nv7ssKGMaG6Og=",
    "zh:01702196f0a0492ec07917db7aaa595843d8f171dc195f4c988d2ffca2a06386",
    "zh:09aae3da826ba3d7df69efeb25d146a1de0d03e951d35019a0f80e4f58c89b53",
    "zh:09ba83c0625b6fe0a954da6fbd0c355ac0b7f07f86c91a2a97849140fea49603",
    "zh:0e3a6c8e16f17f19010accd0844187d524580d9fdb0731f675ffcf4afba03d16",
    "zh:45f2c594b6f2f34ea663704cc72048b212fe7d16fb4cfd959365fa997228a776",
    "zh:77ea3e5a0446784d77114b5e851c970a3dde1e08fa6de38210b8385d7605d451",
    "zh:8a154388f3708e3df5a69122a23bdfaf760a523788a5081976b3d5616f7d30ae",
    "zh:992843002f2db5a11e626b3fc23dc0c87ad3729b3b3cff08e32ffb3df97edbde",
    "zh:ad906f4cebd3ec5e43d5cd6dc8f4c5c9cc3b33d2243c89c5fc18f97f7277b51d",
    "zh:c979425ddb256511137ecd093e23283234da0154b7fa8b21c2687182d9aea8b2",
  ]
}
