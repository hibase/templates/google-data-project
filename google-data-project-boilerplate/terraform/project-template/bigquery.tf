resource random_id "bigquery_stage" {
  byte_length = 3
}

resource google_storage_bucket "bigquery_stage" {
  name = "bigquery-stage-${random_id.bigquery_stage.hex}"
  location = "EU"
  force_destroy = true
}

resource google_storage_bucket_iam_member "bigquery_stage" {
  bucket = google_storage_bucket.bigquery_stage.name
  role = "roles/storage.admin"
  member = "serviceAccount:${module.airflow_service_account.email}"
}

resource google_secret_manager_secret "bigquery_storage_bucket" {
  secret_id = "af-var-bigquery_storage_bucket"
  replication {
    user_managed {
      replicas { location = local.region }
    }
  }
}

resource google_secret_manager_secret_version "bigquery_storage_bucket" {
  secret = google_secret_manager_secret.bigquery_storage_bucket.id
  secret_data = google_storage_bucket.bigquery_stage.name
}
