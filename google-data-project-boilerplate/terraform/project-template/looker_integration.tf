# locals {
#   looker_integration_roles = ["roles/bigquery.dataEditor", "roles/bigquery.jobUser"]
# }

# module "looker_integration_service_account" {
#   source = "../modules/google-service-account"

#   account_id = "looker"
#   display_name = "Looker BigQuery SA"
#   description = "Service Account to connect Looker to BigQuery"
#   create_key = true
#   provide_key_in_secrets_manager = true
# }

# resource google_project_iam_member "looker_integration_service_account" {
#   for_each = toset(local.looker_integration_roles)
#   role = each.value
#   member = "serviceAccount:${module.looker_integration_service_account.email}"
# }

# resource google_bigquery_dataset "looker_integration_scratch" {
#   dataset_id    = "looker_scratch"
#   friendly_name = "Looker Scratch"
#   description   = "Managed by Terraform. Do not edit or delete."
#   location      = "EU"

#   access {
#     role          = "OWNER"
#     user_by_email = module.looker_integration_service_account.email
#   }
# }

