locals {
  airflow_service_account_id = "airflow-sa-${random_id.airflow_service_account.hex}"
  airflow_service_account_conn = "google_cloud_default"
  airflow_service_account_secret = "af-conn-${local.airflow_service_account_conn}"
  airflow_service_account_mount_path = "/secrets/google_application_credentials.json"
  airflow_service_account_roles = [
    "roles/secretmanager.secretAccessor",
    "roles/bigquery.admin"
  ]
}

resource random_id "airflow_service_account" {
  byte_length = 3
}

module "airflow_service_account" {
  source = "../modules/google-service-account"
  account_id = local.airflow_service_account_id
  description = "Airflow SA to use BigQuery, GCS"
  create_key = true
  secret_location = local.region
}

resource google_project_iam_member "airflow_service_account" {
  for_each = toset(local.airflow_service_account_roles)
  role = each.value
  member = "serviceAccount:${module.airflow_service_account.email}"
}

resource google_secret_manager_secret "airflow_service_account" {
  secret_id = local.airflow_service_account_secret
  replication {
    user_managed {
      replicas { location = "europe-west1" }
    }
  }
}

resource google_secret_manager_secret_version "airflow_service_account" {
  secret = google_secret_manager_secret.airflow_service_account.id
  secret_data = base64decode(module.airflow_service_account.private_key)
}

resource kubernetes_secret "airflow_service_account" {
  metadata {
    name = "airflow-gcp-sa"
  }

  data = {
    basename(local.airflow_service_account_mount_path) = base64decode(module.airflow_service_account.private_key)
  }
}

