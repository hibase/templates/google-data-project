module "gcr" {
  source = "../modules/google-container-registry"
}

output "gcr__registry_base" {
  value = module.gcr.registry_base
}
