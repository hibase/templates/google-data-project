module "airflow" {
  source = "../modules/google-airflow-on-kubernetes"
  providers = {
    kubernetes = kubernetes
    helm = helm
  }

  google_sourcerepo_name = google_sourcerepo_repository.repositories["data/airflow"].name
  google_sourcerepo_url = google_sourcerepo_repository.repositories["data/airflow"].url

  image_registry = "gcr.io/${data.google_project.current.project_id}"
  image_name = "airflow"
  image_tag = "2.0.1"

  database_host = module.airflow_postgresql.host
  database_user = module.airflow_postgresql.username
  database_password = module.airflow_postgresql.password
  database_name = module.airflow_postgresql.database

  extra_env = {
    "AIRFLOW_VAR_ENV" = "live"
    "GOOGLE_APPLICATION_CREDENTIALS" = local.airflow_service_account_mount_path
    "AIRFLOW__SECRETS__BACKEND" = "airflow.secrets.local_filesystem.LocalFilesystemBackend"
    "AIRFLOW__SECRETS__BACKEND_KWARGS" = jsonencode({
      "variables_file_path" = "/home/airflow/${local.airflow_variables_filename}"
      "connections_file_path" = "/home/airflow/${local.airflow_connections_filename}"
    })
    "AIRFLOW_MIN_FILE_PROCESS_INTERVAL" = "0"
    "AIRFLOW_DAG_DIR_LIST_INTERVAL" = "10"
    "AIRFLOW__LOGGING__REMOTE_LOGGING" = "True"
    "AIRFLOW__LOGGING__REMOTE_BASE_LOG_FOLDER" = "gs://${google_storage_bucket.airflow_logs.name}"
    "AIRFLOW__WEBSERVER__WORKERS" = "2"
    "AIRFLOW_CONN_GOOGLE_CLOUD_PLATFORM" = "google-cloud-platform://?extra__google_cloud_platform__project=${data.google_project.current.project_id}"
    "AIRFLOW_CONN_GOOGLE_CLOUD_DEFAULT" = "google-cloud-platform://?extra__google_cloud_platform__project=${data.google_project.current.project_id}"
    "AIRFLOW_CONN_BIGQUERY_DEFAULT" = "google-cloud-platform://?extra__google_cloud_platform__project=${data.google_project.current.project_id}"
    "AIRFLOW__KUBERNETES__DELETE_WORKER_PODS" = "True"
    "AIRFLOW__EMAIL__SUBJECT_TEMPLATE" = "/home/airflow/plugins/email_subject_template.txt.jinja2"
    "AIRFLOW__EMAIL__HTML_CONTENT_TEMPLATE" = "/home/airflow/plugins/email_html_content_template.html.jinja2"
    # "AIRFLOW__EMAIL__EMAIL_BACKEND" = "airflow.utils.email.send_email_smtp"
    # "AIRFLOW__SMTP__SMTP_HOST" = ""
    # "AIRFLOW__SMTP__SMTP_PORT" = ""
    # "AIRFLOW__SMTP__SMTP_USER" = ""
    # "AIRFLOW__SMTP__SMTP_MAIL_FROM" = ""
    # "AIRFLOW__SMTP__SMTP_PASSWORD" = data.google_secret_manager_secret_version.smtp_password.secret_data
    "AIRFLOW__SMTP__STARTTLS" = "True"
    "AIRFLOW__SMTP__SMTP_SSL" ="False"
 }

  extra_volumes = [
    {
      "name" = "af-variables"
      "secret" = {
        "secretName" = kubernetes_secret.airflow_variables.metadata[0].name
      }
    },
    {
      "name" = "af-connections"
      "secret" = {
        "secretName" = kubernetes_secret.airflow_connections.metadata[0].name
      }
    },
    {
      "name" = "af-gcp-sa"
      "secret" = {
        "secretName" = kubernetes_secret.airflow_service_account.metadata[0].name
      }
    },
  ]

  extra_volume_mounts = [
    {
      "name" = "af-variables"
      "mountPath" = "/home/airflow/${local.airflow_variables_filename}"
      "subPath" = local.airflow_variables_filename
    },
    {
      "name" = "af-connections"
      "mountPath" = "/home/airflow/${local.airflow_connections_filename}"
      "subPath" = local.airflow_connections_filename
    },
    {
      "name" = "af-gcp-sa"
      "mountPath" = local.airflow_service_account_mount_path
      "subPath" = basename(local.airflow_service_account_mount_path)
    },
  ]

  values = [
    yamlencode({
      web = {
        tolerations = [
          {
            key = "dedicated"
            operator = "Equal"
            value = "airflow-system"
            effect = "NoSchedule"
          }
        ]
      }
      scheduler = {
        tolerations = [
          {
            key = "dedicated"
            operator = "Equal"
            value = "airflow-system"
            effect = "NoSchedule"
          }
        ]
      }
    })
  ]
}

output "airflow__username" {
  sensitive = true
  value = module.airflow.username
}

output "airflow__password" {
  sensitive = true
  value = module.airflow.password
}

