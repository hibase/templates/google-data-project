locals {
  kubernetes_location = element(sort(data.google_compute_zones.europe_west1.names), 0)
}

provider kubernetes {
  host                   = module.airflow_cluster.endpoint
  cluster_ca_certificate = module.airflow_cluster.cluster_ca_certificate
  token                  = data.google_client_config.default.access_token
}

provider helm {
  kubernetes {
    host                   = module.airflow_cluster.endpoint
    cluster_ca_certificate = module.airflow_cluster.cluster_ca_certificate
    token                  = data.google_client_config.default.access_token
  }
}

module "airflow_cluster" {
  source = "../modules/google-kubernetes-cluster"

  name                        = "tld-company-airflow"
  location                    = local.kubernetes_location
  network                     = data.google_compute_network.default.self_link
  subnetwork                  = data.google_compute_subnetwork.default_public.self_link
  enable_private_endpoint     = false
  enable_private_nodes        = false
  cluster_autoscaling_enabled = false
  master_trusted_cidr_blocks  = {
    # "hibase-office-vpn" = "63.33.72.215/32"
  }

  node_pools = [
    {
      name           = "default-2"
      machine_type   = "n1-standard-2"
      min_node_count = 1
      max_node_count = 5
      disk_type      = "pd-ssd"
      disk_size_gb   = 20
      dedicated      = "airflow-system"
    },
    {
      name           = "workers"
      preemptible    = true
      machine_type   = "n1-standard-1"
      min_node_count = 0
      max_node_count = 5
      disk_type      = "pd-ssd"
      disk_size_gb   = 20
    }
  ]

  default_limit_range = {
    "Container" = {
      cpu = "1"
      memory = "1Gi"
    }
  }
}

output "airflow_cluster__kubeconfig" {
  sensitive = true
  value     = module.airflow_cluster.kubeconfig
}
