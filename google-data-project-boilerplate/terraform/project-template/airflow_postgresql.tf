module "airflow_postgresql" {
  source = "../modules/google-cloudsql-postgresql"

  name = "tld-company-airflow"
  database_name = "airflow"
  tier = "db-custom-2-3840"
  region = local.region
  network = data.google_compute_network.default.id
}

output "airflow_postgresql__username" {
  sensitive = true
  value = module.airflow_postgresql.username
}

output "airflow_postgresql__password" {
  sensitive = true
  value = module.airflow_postgresql.password
}

output "airflow_postgresql__database" {
  value = module.airflow_postgresql.database
}

