#!/bin/sh
set -ex

# Reset initial password
echo "$USER:$USER" | sudo chpasswd
# Expire initial password, enforce change on next login
sudo passwd --expire "$USER"
# Remove credentials
rm -rf ~/.gcloud
# Remove browser history
rm -rf ~/snap/chromium/common/.cache/*
